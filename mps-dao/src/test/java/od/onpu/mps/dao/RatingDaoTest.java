package od.onpu.mps.dao;

import junit.framework.Assert;
import od.onpu.mps.dto.StudentRatingDto;
import od.onpu.mps.entity.Rating;
import od.onpu.mps.entity.RatingType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 13.05.13
 * Time: 23:54
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/META-INF/spring/spring-dao.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class RatingDaoTest {

    @Autowired
    RatingDao ratingDao;

    @Autowired
    StudentDao studentDao;

    @Autowired
    PairDao pairDao;

    @Test
    public void testRatingSave(){

        Rating rating = new Rating();

        rating.setPair(pairDao.getAll().get(0));
        rating.setRatingDate(new Date());
        rating.setType(RatingType.REGULAR);
        rating.setStudent(studentDao.getAll().get(0));

        ratingDao.save(rating);
    }

}

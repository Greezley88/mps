/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.dao;

import java.util.List;
import static org.junit.Assert.*;

import od.onpu.mps.entity.Group;
import od.onpu.mps.entity.Student;
import od.onpu.mps.entity.Teacher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author maxim
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/META-INF/spring/spring-dao.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class StudentDaoTest {
    
    @Autowired
    private TeacherDao teacherDao;
    
    @Autowired
    private GroupDao groupDao;
    
    @Autowired
    private StudentDao studentDao;
    
    private Teacher teacher;
    private Group group;
    
    @Before
    public void before(){
        teacher = new Teacher();
        teacher.setName("Test1");
        teacher.setPatronymic("Test1");
        teacher.setSurname("Test1");
        teacher.setRank("Test1");
        
        teacher = teacherDao.update(teacher);
        
        group = new Group();
        group.setName("Ae");
        group.setCurator(teacher);
        
        group = groupDao.update(group);
    }
    
    @After
    public void after(){
        teacherDao.delete(teacher);
        groupDao.delete(group);
    }
    
    @Test
    public void testAddStudent(){
        Student student = new Student();
        student.setGroup(group);
        student.setName("Student");
        student.setNumber("124342");
        student.setPatronymic("Patr");
        student.setSurname("Surn");
        studentDao.update(student);
        int count = studentDao.getAll().size();
        assertFalse(count == 0);
    }
    
    @Test
    public void testDeleteStudent(){
        Student student = new Student();
        student.setGroup(group);
        student.setName("Student");
        student.setNumber("124342");
        student.setPatronymic("Patr");
        student.setSurname("Surn");
        studentDao.update(student);
        int count = studentDao.getAll().size();
        studentDao.delete(student);
        int count1 = studentDao.getAll().size();
        assertTrue(count == count1);
    }
    
}

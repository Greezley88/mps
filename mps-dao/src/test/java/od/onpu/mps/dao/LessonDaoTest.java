/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.dao;

import static org.junit.Assert.*;

import od.onpu.mps.entity.Lesson;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author maxim
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/META-INF/spring/spring-dao.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class LessonDaoTest {
    
    @Autowired
    private LessonDao lessonDao;
    
    @Test
    public void testAddLesson(){
        Lesson lesson = new Lesson();
        lesson.setName("Lesson");
        
        lesson = lessonDao.update(lesson);
        int count = lessonDao.getAll().size();
        assertFalse(count == 0);
    }
    
    @Test
    public void testDeleteLesson(){
        Lesson lesson = new Lesson();
        lesson.setName("Lesson");
        
        int count = lessonDao.getAll().size();
        lesson = lessonDao.update(lesson);
        
        lessonDao.delete(lesson);
        int count1 = lessonDao.getAll().size();
        assertTrue(count == count1);
    }
}

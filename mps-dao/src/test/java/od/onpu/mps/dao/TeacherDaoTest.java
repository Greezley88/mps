/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.dao;

import static org.junit.Assert.*;

import od.onpu.mps.entity.Teacher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author maxim
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/META-INF/spring/spring-dao.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class TeacherDaoTest {
    
    @Autowired
    private TeacherDao teacherDao;
    
    @Test
    public void testAddTeacher(){
        Teacher teacher = new Teacher();
        teacher.setName("Test1");
        teacher.setPatronymic("Test1");
        teacher.setSurname("Test1");
        teacher.setRank("Test1");
        
        teacher = teacherDao.update(teacher);
        System.out.println("Teacher id = " + teacher.getId());
        int count = teacherDao.getAll().size();
        assertFalse(count == 0);
    }
    
    @Test
    public void testDeleteTeacher(){
        Teacher teacher = new Teacher();
        teacher.setName("Test1");
        teacher.setPatronymic("Test1");
        teacher.setSurname("Test1");
        teacher.setRank("Test1");
        
        int count = teacherDao.getAll().size();
        System.out.println("count = " + count);
        
        teacher = teacherDao.update(teacher);
        
        teacherDao.delete(teacher);
        
        int count1 = teacherDao.getAll().size();
        System.out.println("count1 = " + count1);
        
        assertTrue(count == count1);
    }
}

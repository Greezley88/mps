/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.dao;

import od.onpu.mps.entity.Group;
import od.onpu.mps.entity.Lesson;
import od.onpu.mps.entity.Pair;
import od.onpu.mps.entity.Teacher;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author maxim
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/META-INF/spring/spring-dao.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class PairDaoTest {
    
    @Autowired
    private TeacherDao teacherDao;
    
    @Autowired
    private GroupDao groupDao;
    
    @Autowired
    private LessonDao lessonDao;
    
    @Autowired
    private PairDao pairDao;
    
    private Teacher teacher;
    private Group group;
    private Lesson lesson;
    
    @Before
    public void before(){
        teacher = new Teacher();
        teacher.setName("Test1");
        teacher.setPatronymic("Test1");
        teacher.setSurname("Test1");
        teacher.setRank("Test1");

        teacher = teacherDao.update(teacher);

        group = new Group();
        group.setName("Ae");
        group.setCurator(teacher);

        group = groupDao.update(group);

        lesson = new Lesson();
        lesson.setName("Lesson");
        lesson = lessonDao.update(lesson);
    }

    @After
    public void after(){
        teacherDao.delete(teacher);
        groupDao.delete(group);
        lessonDao.delete(lesson);
    }

    @Test
    public void testAddPair(){
        Pair pair = new Pair();
        pair.setGroup(group);
        pair.setLesson(lesson);
        pair.setNumber(1);
        pair.setParity(0);
        pair.setSemester(null);
        pair.setTeacher(teacher);
        pair = pairDao.update(pair);
        int count = pairDao.getAll().size();
        assertFalse(count == 0);
    }

    @Test
    public void testDeletePair(){
        Pair pair = new Pair();
        pair.setGroup(group);
        pair.setLesson(lesson);
        pair.setNumber(1);
        pair.setParity(1);
        pair.setSemester(null);
        pair.setTeacher(teacher);
        int count = pairDao.getAll().size();
        pair = pairDao.update(pair);

        pairDao.delete(pair);
        int count1 = pairDao.getAll().size();
        assertTrue(count == count1);
    }

}

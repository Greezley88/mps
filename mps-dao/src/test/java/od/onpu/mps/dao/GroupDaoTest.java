/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.dao;

import static org.junit.Assert.*;

import od.onpu.mps.entity.Group;
import od.onpu.mps.entity.Teacher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author maxim
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/META-INF/spring/spring-dao.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class GroupDaoTest {
    
    @Autowired
    private TeacherDao teacherDao;
    
    @Autowired
    private GroupDao groupDao;
    
    @Test
    public void testAddGroup(){
        Teacher teacher = new Teacher();
        teacher.setName("Test1");
        teacher.setPatronymic("Test1");
        teacher.setSurname("Test1");
        teacher.setRank("Test1");
        
        teacher = teacherDao.update(teacher);
        
        Group group = new Group();
        group.setName("Ae");
        group.setCurator(teacher);
        
        group = groupDao.update(group);
        int count = groupDao.getAll().size();
        assertFalse(count == 0);
    }
    
    @Test
    public void testDeleteGroup(){
        Teacher teacher = new Teacher();
        teacher.setName("Test1");
        teacher.setPatronymic("Test1");
        teacher.setSurname("Test1");
        teacher.setRank("Test1");
        
        teacher = teacherDao.update(teacher);
        
        Group group = new Group();
        group.setName("Ae");
        group.setCurator(teacher);
        
        int count = groupDao.getAll().size();
        group = groupDao.update(group);
        groupDao.delete(group);
        int count1 = groupDao.getAll().size();
        assertEquals(count, count1);
    }
    
    @Test(expected = javax.persistence.PersistenceException.class)
    public void testAddGroupWithInvalidCurator(){
        Teacher teacher = new Teacher();
        teacher.setName("Test1");
        teacher.setPatronymic("Test1");
        teacher.setSurname("Test1");
        teacher.setRank("Test1");
        
        teacher = teacherDao.update(teacher);
        
        teacherDao.delete(teacher);
        teacherDao.getAll();
        
        Group group = new Group();
        group.setName("Ae");
        group.setCurator(teacher);
        
        group = groupDao.update(group);
    }
}

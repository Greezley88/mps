/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.dao.impl;

import od.onpu.mps.dao.PairDao;
import od.onpu.mps.entity.Pair;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

/**
 *
 * @author maxim
 */
@Repository("pairDao")
public class PairJpaDao extends GenericJpaDao<Pair> implements PairDao{
        
    public PairJpaDao(){
        super(Pair.class);
    }

    public PairJpaDao(Class<Pair> type) {
        super(type);
    }


    private final String GET_SCHEDULE_FOR_GROUP_AND_SEMESTER = "SELECT p FROM Pair p WHERE p.semester.id=:semester_id " +
            "AND p.group.id=:group_id ORDER BY p.weekDay,p.number,p.parity";

    private final String GET_PAIRS_FOR_STUDY_SHEET = "SELECT p FROM Pair p WHERE p.semester.id=:semester " +
            "AND p.teacher.id=:teacher " +
            "AND p.lesson.id=:lesson " +
            "AND p.group.id=:group " +
            "AND not exists(SELECT r FROM Rating r WHERE r.pair=p)";

    @Override
    public List<Pair> getSchedule(Integer semesterId,Integer groupId){


        Query query = entityManager.createQuery(GET_SCHEDULE_FOR_GROUP_AND_SEMESTER);
        query.setParameter("semester_id",semesterId);
        query.setParameter("group_id",groupId);

        return query.getResultList();
    }

    // get pair (pairs) for generation sheet of balls
    @Override
    public List<Pair> getPairsForStudySheet(Integer semester,Integer teacher,Integer lesson,Integer group){

        Query query = entityManager.createQuery(GET_PAIRS_FOR_STUDY_SHEET);
        query.setParameter("semester",semester);
        query.setParameter("teacher",teacher);
        query.setParameter("lesson",lesson);
        query.setParameter("group",group);

        return query.getResultList();
    }

    @Override
    public List<Pair> getPairsForGroup(Integer semesterId,Integer groupId){

        Query query = entityManager.createQuery("SELECT p FROM Pair p WHERE p.group.id=:group_id AND p.semester.id=:semester_id GROUP BY p.lesson,p.teacher");
        query.setParameter("semester_id",semesterId);
        query.setParameter("group_id",groupId);

        return query.getResultList();
    }

    @Override
    public List<Pair> getPairsForTeacher(Integer semesterId,Integer groupId, Integer teacherId){

        Query query = entityManager.createQuery("SELECT p FROM Pair p WHERE p.group.id=:group_id AND p.semester.id=:semester_id AND p.teacher.id=:teacher_id");
        query.setParameter("semester_id",semesterId);
        query.setParameter("group_id",groupId);
        query.setParameter("teacher_id",teacherId);

        return query.getResultList();
    }
}

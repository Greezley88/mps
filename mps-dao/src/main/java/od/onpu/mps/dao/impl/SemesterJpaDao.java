package od.onpu.mps.dao.impl;

import od.onpu.mps.dao.SemesterDao;
import od.onpu.mps.entity.Semester;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 06.05.13
 * Time: 20:21
 * To change this template use File | Settings | File Templates.
 */
@Repository("semesterDao")
public class SemesterJpaDao extends GenericJpaDao<Semester> implements SemesterDao {

    public SemesterJpaDao(Class<Semester> type) {
        super(type);
    }

    public SemesterJpaDao(){
        super(Semester.class);
    }

    private final String ALL_SEMESTERS_FOR_GROUP = "SELECT DISTINCT s FROM Semester s,Pair p,Group g WHERE p.group=g AND g.id=:id AND s=p.semester ORDER BY s.startDate";

    @Override
    public List<Semester> getSemestersForGroup(Integer id){

        Query query = entityManager.createQuery(ALL_SEMESTERS_FOR_GROUP);
        query.setParameter("id",id);

        return query.getResultList();
    }
}

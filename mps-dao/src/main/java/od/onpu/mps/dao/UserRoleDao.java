package od.onpu.mps.dao;

import od.onpu.mps.entity.UserRole;

/**
 * Created with IntelliJ IDEA.
 * User: Станислав
 * Date: 5/18/13
 * Time: 2:54 PM
 * To change this template use File | Settings | File Templates.
 */
public interface UserRoleDao extends GenericDao<UserRole>
{
    public UserRole getUserRoleByUserId(Long id);

    public UserRole getRole(String role);
}

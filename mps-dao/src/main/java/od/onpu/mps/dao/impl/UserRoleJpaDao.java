package od.onpu.mps.dao.impl;

/**
 * Created with IntelliJ IDEA.
 * User: StasMenshykov
 * Date: 5/18/13
 * Time: 2:56 PM
 * To change this template use File | Settings | File Templates.
 */

import od.onpu.mps.dao.UserRoleDao;
import od.onpu.mps.entity.UserRole;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;

@Repository("userRoleDao")
public class UserRoleJpaDao extends GenericJpaDao<UserRole> implements UserRoleDao {

    UserRoleJpaDao(){
        super(UserRole.class);
    }

    @Override
    public UserRole getUserRoleByUserId(Long id)
    {
        Query query = entityManager.createQuery("select userRole from UserRole userRole where user_id = :id");
        query.setParameter("id", id);
        return (UserRole)query.getSingleResult();
    }

    @Override
    public UserRole getRole(String role){
        Query query = entityManager.createQuery("select obj from UserRole obj where obj.role = :role");
        query.setParameter("role", role);
        return (UserRole)query.getSingleResult();
    }
}

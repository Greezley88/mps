/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.dao;

import od.onpu.mps.entity.Lesson;

import java.util.List;

/**
 *
 * @author maxim
 */
public interface LessonDao extends GenericDao<Lesson>{

    List<Lesson> getLessonsForGroup(Integer groupId, Integer semesterId);
}

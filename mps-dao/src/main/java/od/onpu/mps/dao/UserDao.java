package od.onpu.mps.dao;

import od.onpu.mps.entity.User;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: StasMenshykov
 * Date: 5/18/13
 * Time: 2:06 PM
 * To change this template use File | Settings | File Templates.
 */
public interface UserDao extends  GenericDao<User>
{
    public User getUserByLogin(String login);

}

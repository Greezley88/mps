package od.onpu.mps.dao.impl;

import od.onpu.mps.dao.RatingDao;
import od.onpu.mps.dto.StudentRatingDto;
import od.onpu.mps.entity.Rating;
import od.onpu.mps.entity.RatingType;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 13.05.13
 * Time: 23:52
 * To change this template use File | Settings | File Templates.
 */
@Repository("ratingDao")
public class RatingJpaDao extends GenericJpaDao<Rating> implements RatingDao{

    public RatingJpaDao(Class<Rating> type) {
        super(type);
    }

    public RatingJpaDao(){
        super(Rating.class);
    }

    private final String GET_STUDENTS_RATING_FOR_GROUP = "SELECT new "+ StudentRatingDto.class.getName()+"(s) "+
            "FROM  Student s " +
            "WHERE s.group.id=:groupId";

    private final String GET_RATING_FOR_PAIR = "SELECT r FROM Rating r WHERE r.student.id=:studentId AND r.pair.id IN " +
            "(SELECT p.id FROM Pair p,Pair p2 WHERE p2.lesson=p.lesson " +
            "AND p.group=p2.group AND p.teacher=p2.teacher AND p.semester=p2.semester AND p2.id=:pairId)";

    private final String UPDATE_RATING_TYPE = "UPDATE Rating r SET r.type=:ratingType WHERE r.pair.id IN " +
            "(SELECT p.id FROM Pair p,Pair p2 WHERE p2.lesson=p.lesson " +
            "AND p.group=p2.group AND p.teacher=p2.teacher AND p.semester=p2.semester AND p2.id=:pairId)";

    private final String GET_FINAL_RATING_FOR_STUDENT = "SELECT r FROM Rating r WHERE r.type=:ratingType " +
            "AND r.pair.id IN (SELECT p.id FROM Pair p WHERE p.group.id=:groupId AND p.semester.id=:semesterId) " +
            "AND r.student.id=:studentId " +
            "ORDER BY r.pair.id";

    private final String FILTER_BY_PAIR =  " r.pair IN " +
            "(SELECT p FROM Pair p,Pair p2 WHERE p2.lesson=p.lesson " +
            "AND p.group=p2.group AND p.teacher=p2.teacher AND p2.id=:pairId)";

    protected List<StudentRatingDto> fillDtoByStudents(Integer groupId){
        Query query = entityManager.createQuery(GET_STUDENTS_RATING_FOR_GROUP);
        query.setParameter("groupId",groupId);
        return query.getResultList();
    }


    public List<Rating> getRatingForStudentByPair(Integer studentId,Integer pairId){

        Query query = entityManager.createQuery(GET_RATING_FOR_PAIR);
        query.setParameter("studentId",studentId);
        query.setParameter("pairId",pairId);
        return query.getResultList();
    }

    public List<Rating> getFinalRatingForStudent(Integer studentId,Integer groupId,Integer semesterId){

        Query query = entityManager.createQuery(GET_FINAL_RATING_FOR_STUDENT);
        query.setParameter("ratingType",RatingType.FINAL);
        query.setParameter("studentId",studentId);
        query.setParameter("groupId",groupId);
        query.setParameter("semesterId",semesterId);
        return query.getResultList();
    }

    @Override
    public List<StudentRatingDto> getStudentsRatingForPair(Integer groupId,Integer pairId){

        List<StudentRatingDto> studentRating = fillDtoByStudents(groupId);
        for (StudentRatingDto studentDto : studentRating) {
            studentDto.getRating().addAll(getRatingForStudentByPair(studentDto.getStudent().getId(),pairId));
        }

        return studentRating;
    }

    @Override
    public List<StudentRatingDto> getFinalRatingForGroup(Integer groupId,Integer semesterId){
        List<StudentRatingDto> studentRating = fillDtoByStudents(groupId);
        for (StudentRatingDto studentDto : studentRating) {
            studentDto.getRating().addAll(getFinalRatingForStudent(studentDto.getStudent().getId(),groupId,semesterId));
        }
        return studentRating;
    }

    @Override
    public Integer updateRatingType(Integer[] modular, Integer finalRating,Integer pairId){
        int result = 0;
        // clear old information: set all rating to REGULAR
        Query query = entityManager.createQuery(UPDATE_RATING_TYPE);
        query.setParameter("ratingType", RatingType.REGULAR);
        query.setParameter("pairId",pairId);
        result+=query.executeUpdate();

        StringBuilder queryBuilder = new StringBuilder(UPDATE_RATING_TYPE);
        queryBuilder.append(" AND r.ratingDate IN(:ratingDate)");
        // set modular rating
        query = entityManager.createQuery(queryBuilder.toString());
        query.setParameter("ratingType", RatingType.MODULAR);
        query.setParameter("pairId",pairId);
        query.setParameter("ratingDate", getRatingDates(modular));
        result+=query.executeUpdate();
         // set final rating
        query = entityManager.createQuery(queryBuilder.toString());
        query.setParameter("ratingType", RatingType.FINAL);
        query.setParameter("pairId",pairId);
        query.setParameter("ratingDate", getRatingDates(new Integer[]{finalRating}));
        result+=query.executeUpdate();

        return result; // affected rows
    }

    public List<Date> getRatingDates(Integer[] ratingId){


        Query query = entityManager.createQuery("SELECT r2.ratingDate FROM Rating r2 WHERE r2.id IN(:ratingId)");
        query.setParameter("ratingId",Arrays.asList(ratingId));

        return query.getResultList();
    }


    @Override
    public Integer updateRating(Integer ratingId, Integer rating) {

        Query query = entityManager.createQuery("UPDATE Rating r SET r.rating=:rating WHERE r.id=:ratingId");

        query.setParameter("rating",rating);
        query.setParameter("ratingId",ratingId);

        return query.executeUpdate();
    }
}

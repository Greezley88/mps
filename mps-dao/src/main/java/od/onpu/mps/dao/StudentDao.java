/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.dao;

import od.onpu.mps.entity.Student;

import java.util.List;

/**
 *
 * @author maxim
 */
public interface StudentDao extends GenericDao<Student>{

    List<Student> getByGroupId(int id);
}

package od.onpu.mps.dao;

import od.onpu.mps.dto.StudentRatingDto;
import od.onpu.mps.entity.Rating;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 13.05.13
 * Time: 23:51
 * To change this template use File | Settings | File Templates.
 */
public interface RatingDao extends GenericDao<Rating> {

    List<StudentRatingDto> getStudentsRatingForPair(Integer groupId, Integer pairId);

    Integer updateRatingType(Integer[] modular, Integer finalRating, Integer pairId);

    List<StudentRatingDto> getFinalRatingForGroup(Integer groupId, Integer semesterId);

    Integer updateRating(Integer ratingId, Integer rating);
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.dao.impl;

import od.onpu.mps.dao.StudentDao;
import od.onpu.mps.entity.Student;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

/**
 *
 * @author maxim
 */
@Repository("studentDao")
public class StudentJpaDao extends GenericJpaDao<Student> implements StudentDao{
    
    public StudentJpaDao(){
        super(Student.class);
    }

    public StudentJpaDao(Class<Student> type) {
        super(type);
    }

    private final String FIND_STUDENTS_BY_GROUP_ID = "SELECT s FROM Student s WHERE s.group.id=:id";

    @Override
    public List<Student> getByGroupId(int id) {

        Query query = entityManager.createQuery(FIND_STUDENTS_BY_GROUP_ID);
        query.setParameter("id",id);

        return query.getResultList();
    }
}

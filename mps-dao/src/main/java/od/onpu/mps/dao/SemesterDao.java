package od.onpu.mps.dao;

import od.onpu.mps.entity.Semester;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 06.05.13
 * Time: 20:21
 * To change this template use File | Settings | File Templates.
 */
public interface SemesterDao extends GenericDao<Semester> {

    public List<Semester> getSemestersForGroup(Integer id);

}

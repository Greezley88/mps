package od.onpu.mps.dao.impl;

import od.onpu.mps.dao.UserDao;
import od.onpu.mps.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: StasMenshykov
 * Date: 5/18/13
 * Time: 2:56 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository("userDao")
public class UserJpaDao extends GenericJpaDao<User> implements UserDao {
    public UserJpaDao() {
        super(User.class);
    }


    public User getUserByLogin(String login) {
        Query query = entityManager.createQuery("select user from User user where login = :login");
        query.setParameter("login", login);
        return (User) query.getSingleResult();
    }
}

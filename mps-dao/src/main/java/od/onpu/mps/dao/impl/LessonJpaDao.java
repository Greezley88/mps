/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.dao.impl;

import od.onpu.mps.dao.LessonDao;
import od.onpu.mps.entity.Lesson;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

/**
 *
 * @author maxim
 */
@Repository("lessonDao")
public class LessonJpaDao extends GenericJpaDao<Lesson> implements LessonDao{
        
    public LessonJpaDao(){
        super(Lesson.class);
    }

    public LessonJpaDao(Class<Lesson> type) {
        super(type);
    }

    private final String GET_LESSONS_FOR_GROUP = "SELECT DISTINCT p.lesson FROM Pair p " +
            "WHERE p.semester.id=:semester_id AND p.group.id=:group_id";

    @Override
    public List<Lesson> getLessonsForGroup(Integer groupId,Integer semesterId){

        Query query = entityManager.createQuery(GET_LESSONS_FOR_GROUP);
        query.setParameter("semester_id",semesterId);
        query.setParameter("group_id",groupId);

        return query.getResultList();
    }
}

package od.onpu.mps.dao.impl;

import od.onpu.mps.dao.GenericDao;
import od.onpu.mps.entity.DomainObject;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 03.04.13
 * Time: 11:57
 * To change this template use File | Settings | File Templates.
 */
@Transactional
public class GenericJpaDao<T extends DomainObject> implements GenericDao<T>  {

    private Class<T> type;

    protected EntityManager entityManager;

    public GenericJpaDao(Class<T> type) {
        super();
        this.type = type;
    }

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public T getById(Integer id) {
        return (T) entityManager.find(type, id);
    }

    @Override
    public List<T> getAll() {
        return entityManager.createQuery(
                "select obj from " + type.getName() + " obj").getResultList();
    }

    @Override
    public void save(T object) {
        try {
            entityManager.persist(object);
        } catch (Exception cve) {
            cve.printStackTrace();
        }

    }

    @Override
    public T update(T cabman) {
        return entityManager.merge(cabman);
    }

    @Override
    public void delete(T object) {
        entityManager.remove(entityManager.merge(object));
    }

    @Override
    public List<T> executeQuery(String query) {

        return entityManager.createQuery(query).getResultList();

    }
}

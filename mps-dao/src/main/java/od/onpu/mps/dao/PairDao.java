/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.dao;

import od.onpu.mps.entity.Pair;

import java.util.List;

/**
 *
 * @author maxim
 */
public interface PairDao extends GenericDao<Pair>{

    List<Pair> getSchedule(Integer semesterId, Integer groupId);

    // get pair (pairs) for generation sheet of evaluations
    List<Pair> getPairsForStudySheet(Integer semester, Integer teacher, Integer lesson, Integer group);

    List<Pair> getPairsForGroup(Integer semesterId, Integer groupId);

    public List<Pair> getPairsForTeacher(Integer semesterId,Integer groupId, Integer teacherId);
}

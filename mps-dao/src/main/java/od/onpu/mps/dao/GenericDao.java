package od.onpu.mps.dao;

import od.onpu.mps.entity.DomainObject;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 03.04.13
 * Time: 11:55
 * To change this template use File | Settings | File Templates.
 */
public interface GenericDao<T extends DomainObject>  {

    public T getById(Integer id);

    public List<T> getAll();

    public void save(T object);

    public T update(T object);

    public void delete(T object);

    public List<T> executeQuery(String query);

}
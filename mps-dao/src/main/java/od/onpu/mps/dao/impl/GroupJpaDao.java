/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.dao.impl;

import od.onpu.mps.dao.GroupDao;
import od.onpu.mps.entity.Group;
import org.springframework.stereotype.Repository;

/**
 *
 * @author maxim
 */
@Repository("groupDao")
public class GroupJpaDao extends GenericJpaDao<Group> implements GroupDao{
        
    public GroupJpaDao(){
        super(Group.class);
    }

    public GroupJpaDao(Class<Group> type) {
        super(type);
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.dao.impl;

import od.onpu.mps.dao.TeacherDao;
import od.onpu.mps.entity.Teacher;
import org.springframework.stereotype.Repository;

/**
 *
 * @author maxim
 */
@Repository("teacherDao")
public class TeacherJpaDao extends GenericJpaDao<Teacher> implements TeacherDao{
    
    public TeacherJpaDao(){
        super(Teacher.class);
    }

    public TeacherJpaDao(Class<Teacher> type) {
        super(type);
    }
}

package od.onpu.mps.dto;

import od.onpu.mps.entity.Rating;
import od.onpu.mps.entity.Student;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 5/14/13
 * Time: 4:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class StudentRatingDto {

    private Student student;
    private List<Rating> rating;

    public StudentRatingDto(){}

    public StudentRatingDto(Student student, List<Rating> rating){
        this.student = student;
        this.rating = rating;
    }

    public StudentRatingDto(Student student){
        this.student = student;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public List<Rating> getRating() {
        if(rating==null){
            rating = new ArrayList<Rating>();
        }
        return rating;
    }

    public void setRating(List<Rating> rating) {
        this.rating = rating;
    }
}

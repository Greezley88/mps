package od.onpu.mps.entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: StasMenshykov
 * Date: 5/18/13
 * Time: 2:07 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "user")
public class User extends DomainObject implements  UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "login", nullable = false)
    private String login;


    @Column(name = "password", nullable = false)
    private String password;


    @Column(name = "enabled", nullable = false)
    private Integer enabled;

    @OneToOne
    @JoinColumn(name = "id_teacher")
    private Teacher teacher;

    @OneToOne
    @JoinColumn(name = "id_student")
    private Student student;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_userRole",
    joinColumns = {@JoinColumn(name = "user_id")},
    inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private List<UserRole> role;

    public enum Status {STUDENT, TEACHER,ORDINARY}

    public User() {
    }

    public User(String login, String password, Integer enabled, Teacher teacher,
                Student student,  List<UserRole> role) {
        this.login = login;
        this.password = password;
        this.enabled = enabled;

        this.teacher= teacher;
        this.student = student;
        this.role = role;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }


    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
    /*
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        ArrayList arrayList = new ArrayList<GrantedAuthority>();
        arrayList.add(this.role);
        return arrayList;
    }
    */

    public List<UserRole> getAuthorities() {
        return role;
    }

    public void setAuthorities(List<UserRole> roles) {
      this.role = roles;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return this.login;
    }


    @Override
    public boolean isAccountNonExpired() {
        return true;
    }


    @Override
    public boolean isAccountNonLocked() {
        return true;
    }


    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }


    @Override
    public boolean isEnabled() {
        return this.enabled!=0;
    }


    public void setPassword(String password) {
        this.password = password;
    }


    public List<UserRole> getRole() {
        return role;
    }

    public void setRole(List<UserRole> role) {
        this.role = role;
    }

    public Status getStatus() {
    if (student != null) return Status.STUDENT;
    else if(teacher != null) return Status.TEACHER;
        else return Status.ORDINARY;
    }

    public String getUserDetails(){
      if(this.getStatus() == Status.TEACHER){
          return String.format("%s,  %s",teacher.getShortFullName(), teacher.getRank());
      }
      else if(this.getStatus() == Status.STUDENT) return String.format("%s, %s",student.getShortFullName(), student.getGroup().getName());
      return this.getLogin();
    }

    public Boolean isTeacher(){

        for(int i=0;i<this.role.size();i++){

            if (this.role.get(i).getAuthority().equals("ROLE_TEACHER")) return true;

        }
        return false;
    }

    public Boolean canEdit(Integer teacherId){
       if(this.isTeacher()) {
           if(this.teacher.getId() == teacherId) return true;
       }
       return false;
    }

    public Boolean isAdmin(){

        for(int i=0;i<this.role.size();i++){

            if (this.role.get(i).getAuthority().equals("ROLE_ADMIN")) return true;

        }
        return false;
    }


    public Boolean[] getRoleFlags(){
        Boolean flags[] = {false,false,false,false};
        List<UserRole> roles = this.getRole();
      for(int i=0;i<roles.size();i++){
        if(roles.get(i).getRole().equals("ROLE_SECRETARY")) flags[0] = true;
        if(roles.get(i).getRole().equals("ROLE_ADMIN")) flags[1] = true;
        if(roles.get(i).getRole().equals("ROLE_TEACHER")) flags[2] = true;
        if(roles.get(i).getRole().equals("ROLE_STUDENT")) flags[3] = true;
      }
        return flags;
    }



}


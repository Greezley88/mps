package od.onpu.mps.entity;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 13.05.13
 * Time: 22:42
 * To change this template use File | Settings | File Templates.
 */
public enum RatingType {
    REGULAR, // usual, common rating

    MODULAR,

    FINAL;
}

package od.onpu.mps.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created with IntelliJ IDEA.
 * User: StasMenshykov
 * Date: 5/28/13
 * Time: 3:16 PM
 * To change this template use File | Settings | File Templates.
 */
//@Entity
//@Table(name="authorities")
public class Authority extends DomainObject {

    private Integer id;

    private String authority;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}

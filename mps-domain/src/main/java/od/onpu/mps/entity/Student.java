/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.entity;

import javax.persistence.*;
import java.util.Set;

/**
 *
 * @author maxim
 */
@Entity
@Table(name = "student")
public class Student extends DomainObject{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "student_id")
    private Integer id;
    
    @Column(nullable = false)
    private String name;
    
    @Column(nullable = false)
    private String surname;
    
    @Column(nullable = false)
    private String patronymic;
    
    @Column(nullable = false)
    private String number;       // student ID or student's record-book
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "groupp", nullable = false)
    private Group group;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getFullName(){
        return String.format("%s %s %s",surname,name,patronymic);
    }


    public String getShortFullName(){
        return String.format("%s %s. %s.",surname,name.substring(0,1),patronymic.substring(0,1));
    }


}

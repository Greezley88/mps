package od.onpu.mps.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 06.05.13
 * Time: 19:45
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="semester")
public class Semester extends  DomainObject{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Temporal(TemporalType.DATE)
    private Date startDate;

    @Temporal(TemporalType.DATE)
    private Date endDate;

    @Transient
    private String name; // value for output semester name

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * Value for output semester name
     * @return String
     */
    public String getName() {
        return name==null ? String.format("%s - %s",startDate,endDate) : name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

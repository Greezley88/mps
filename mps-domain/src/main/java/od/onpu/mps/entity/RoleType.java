package od.onpu.mps.entity;

/**
 * Created with IntelliJ IDEA.
 * User: Станислав
 * Date: 6/7/13
 * Time: 10:31 PM
 * To change this template use File | Settings | File Templates.
 */
public enum RoleType {
    ROLE_STUDENT,

    ROLE_TEACHER,

    ROLE_ADMINISTRATOR;
}

package od.onpu.mps.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 13.05.13
 * Time: 22:58
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "rating")
public class Rating extends DomainObject {

    @Id@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer id;

    @Enumerated(EnumType.STRING)
    private RatingType type;

    private Integer rating;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date ratingDate;

    @OneToOne
    @JoinColumn(name = "student_id",nullable = false)
    private Student student;

    @OneToOne
    @JoinColumn(name = "pair_id",nullable = false)
    private Pair pair;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RatingType getType() {
        return type;
    }

    public void setType(RatingType type) {
        this.type = type;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Date getRatingDate() {
        return ratingDate;
    }

    public void setRatingDate(Date ratingDate) {
        this.ratingDate = ratingDate;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Pair getPair() {
        return pair;
    }

    public void setPair(Pair pair) {
        this.pair = pair;
    }
}

package od.onpu.mps.services;

import od.onpu.mps.entity.DomainObject;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 07.05.13
 * Time: 18:46
 * To change this template use File | Settings | File Templates.
 */
public interface GenericService<T extends DomainObject> {

    public void save(T pair);

    public T update(T pair);

    public void delete(T pair);

    public T getById(Integer id);

    public List<T> getAll();

}

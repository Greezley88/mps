/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.services;

import java.util.List;
import od.onpu.mps.entity.Student;

/**
 *
 * @author maxim
 */
public interface StudentService extends GenericService<Student>{

    public List<Student> getByGroupId(int id);

    public List<Student> getNotUsersByGroupId(int id);
    
}

package od.onpu.mps.services;

import od.onpu.mps.entity.Semester;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 06.05.13
 * Time: 21:32
 * To change this template use File | Settings | File Templates.
 */
public interface SemesterService extends GenericService<Semester>{
    List<Semester> getAllForGroup(Integer id);
}

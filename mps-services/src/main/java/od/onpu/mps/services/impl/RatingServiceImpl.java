package od.onpu.mps.services.impl;

import od.onpu.mps.dao.GenericDao;
import od.onpu.mps.dao.RatingDao;
import od.onpu.mps.dto.StudentRatingDto;
import od.onpu.mps.entity.Pair;
import od.onpu.mps.entity.Rating;
import od.onpu.mps.entity.RatingType;
import od.onpu.mps.entity.Student;
import od.onpu.mps.services.PairService;
import od.onpu.mps.services.RatingService;
import od.onpu.mps.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 14.05.13
 * Time: 9:57
 * To change this template use File | Settings | File Templates.
 */
@Service("ratingService")
public class RatingServiceImpl extends GenericServiceImpl<Rating> implements RatingService {

    @Autowired
    private RatingDao ratingDao;

    @Autowired
    private PairService pairService;

    @Autowired
    private StudentService studentService;

    @Override
    public GenericDao<Rating> getDao() {
        return ratingDao;
    }

    @Override
    public List<Rating> generateRatingList(Pair pair){

        List<Student> students = studentService.getByGroupId(pair.getGroup().getId());
        Set<Date> dates = pairService.getDatesForPair(pair);
        List<Rating> ratingList = new ArrayList<Rating>(students.size()*dates.size());
        for (Date date : dates) {
            for (Student student : students) {
                Rating rating = new Rating();
                rating.setRatingDate(date);
                rating.setType(RatingType.REGULAR);
                rating.setPair(pair);
                rating.setStudent(student);
                ratingDao.save(rating);

                ratingList.add(rating);
            }
        }
        return ratingList;
    }

    @Override
    public List<StudentRatingDto> getStudentsRatingForPair(Integer groupId, Integer pairId) {
        return ratingDao.getStudentsRatingForPair(groupId,pairId);
    }

    @Override
    public List<StudentRatingDto> getFinalRatingForGroup(Integer groupId, Integer semesterId) {
        return ratingDao.getFinalRatingForGroup(groupId,semesterId);
    }

    @Override
    public Integer updateRatingType(Integer[] modular, Integer finalRating, Integer pairId){
       return ratingDao.updateRatingType(modular,finalRating,pairId);
    }

    @Override
    public Integer updateRating(Integer ratingId, Integer rating) {
        return ratingDao.updateRating(ratingId,rating);
    }
}

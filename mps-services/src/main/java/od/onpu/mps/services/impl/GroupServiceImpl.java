/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.services.impl;

import java.util.List;

import od.onpu.mps.dao.GenericDao;
import od.onpu.mps.dao.GroupDao;
import od.onpu.mps.entity.Group;
import od.onpu.mps.services.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author maxim
 */
@Service("groupService")
public class GroupServiceImpl extends GenericServiceImpl<Group> implements GroupService{

    @Autowired
    private GroupDao groupDao;

    @Override
    public GenericDao<Group> getDao() {
        return groupDao;
    }
}

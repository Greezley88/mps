/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.services.impl;

import java.util.List;

import od.onpu.mps.dao.GenericDao;
import od.onpu.mps.dao.StudentDao;
import od.onpu.mps.entity.Student;
import od.onpu.mps.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author maxim
 */
@Service("studentService")
public class StudentServiceImpl extends GenericServiceImpl<Student> implements StudentService{

    @Autowired
    private StudentDao studentDao;

    @Override
    public GenericDao<Student> getDao() {
        return studentDao;
    }

    @Override
    public List<Student> getByGroupId(int id) {
        return studentDao.getByGroupId(id);
    }

    @Override
    public List<Student> getNotUsersByGroupId(int gid){
        String query = String.format("select s from Student s where s.group.id = '%s' and s.id not in(select u.student from User u)" , gid);
        return studentDao.executeQuery(query);
    }
}

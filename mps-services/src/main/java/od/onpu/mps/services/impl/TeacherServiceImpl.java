/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.services.impl;

import java.util.List;

import od.onpu.mps.dao.GenericDao;
import od.onpu.mps.dao.TeacherDao;
import od.onpu.mps.entity.Teacher;
import od.onpu.mps.services.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author maxim
 */
@Service("teacherService")
public class TeacherServiceImpl extends GenericServiceImpl<Teacher> implements TeacherService{

    @Autowired
    private TeacherDao teacherDao;

    @Override
    public GenericDao<Teacher> getDao() {
        return teacherDao;
    }

    @Override
    public List<Teacher> getNotUsers(){
        return teacherDao.executeQuery("select t from Teacher t where t.id not in(select s.teacher from User s)");
    }
}

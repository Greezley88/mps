package od.onpu.mps.services.impl;

import od.onpu.mps.dao.GenericDao;
import od.onpu.mps.dao.SemesterDao;
import od.onpu.mps.entity.Semester;
import od.onpu.mps.services.SemesterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 06.05.13
 * Time: 21:33
 * To change this template use File | Settings | File Templates.
 */
@Service("scheduleService")
public class SemesterServiceImpl extends GenericServiceImpl<Semester> implements SemesterService {

    @Autowired
    private SemesterDao semesterDao;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");

    @Override
    public List<Semester> getAllForGroup(Integer id){
        List<Semester> semesters = semesterDao.getSemestersForGroup(id);
        // create readable and formatted name for semesters
        for (Semester sem: semesters){
            sem.setName(String.format("%s - %s",simpleDateFormat.format(sem.getStartDate()),simpleDateFormat.format(sem.getEndDate())));
        }
        return semesters;
    }

    public void getSchedule(Semester semester,Integer groupId){




    }

    @Override
    public GenericDao<Semester> getDao() {
        return semesterDao;
    }
}

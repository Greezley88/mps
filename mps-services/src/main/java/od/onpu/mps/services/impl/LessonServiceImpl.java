/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.services.impl;

import java.util.List;

import od.onpu.mps.dao.GenericDao;
import od.onpu.mps.dao.LessonDao;
import od.onpu.mps.entity.Lesson;
import od.onpu.mps.services.LessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author maxim
 */
@Service("lessonService")
public class LessonServiceImpl extends GenericServiceImpl<Lesson> implements LessonService{

    @Autowired
    private LessonDao lessonDao;


    @Override
    public GenericDao<Lesson> getDao() {
        return lessonDao;
    }

    @Override
    public List<Lesson> getLessonsForGroup(Integer groupId,Integer semesterId){
             return lessonDao.getLessonsForGroup(groupId,semesterId);
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.services.impl;

import java.util.*;

import od.onpu.mps.dao.GenericDao;
import od.onpu.mps.dao.PairDao;
import od.onpu.mps.entity.Pair;
import od.onpu.mps.entity.User;
import od.onpu.mps.services.PairService;
import od.onpu.mps.services.UserService;
import od.onpu.mps.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author maxim
 */
@Service("pairService")
public class PairServiceImpl extends GenericServiceImpl<Pair> implements PairService{

    @Autowired
    private PairDao pairDao;

    @Autowired
    UserService userService;


    @Override
    public GenericDao<Pair> getDao() {
        return pairDao;
    }


    @Override
    public Map<Integer, List<Pair>> getSchedule(Integer semesterId, Integer groupId){

        List<Pair> pairsInSchedule = pairDao.getSchedule(semesterId,groupId);

        // create map structure for schedule:
        // weekDay => schedule
        // 1 => {MPS,SPO}, 2=>{SPO,Fiz-Ra} etc.
        Map<Integer,List<Pair>> schedule = new LinkedHashMap<Integer, List<Pair>>();
        for (Pair p: pairsInSchedule){

            if(schedule.containsKey(p.getWeekDay())){
                schedule.get(p.getWeekDay()).add(p);
            }else{
                List<Pair> list = new ArrayList<Pair>(5);
                list.add(p);
                schedule.put(p.getWeekDay(),list);
            }
        }

        return schedule;
    }

    @Override
    public List<Pair> getPairsForStudySheet(Integer semester, Integer teacher, Integer lesson, Integer group){

        return pairDao.getPairsForStudySheet(semester,teacher,lesson,group);
    }

    @Override
    public List<Pair> getPairsForGroup(Integer semesterId,Integer groupId){
        User user = userService.getCurrentUser();
        if(user != null) if(!user.getRoleFlags()[0]) return pairDao.getPairsForTeacher(semesterId, groupId, user.getTeacher().getId());
        return pairDao.getPairsForGroup(semesterId,groupId);

    }

    @Override
    public Set<Date> getDatesForPair(Pair pair){
        if(pair.getSemester()==null){
            return Collections.EMPTY_SET;
        }
        List<Pair> onePair = new ArrayList<Pair>();
        onePair.add(pair);
        return generateDatesForPair(onePair);
    }

    protected Set<Date> generateDatesForPair(List<Pair> pairs){

        Set<Date> pairDates = new TreeSet<Date>(); // use TreeSet, that implements SortedSet
        for(Pair p:pairs){
            Date dateFrom = p.getSemester().getStartDate();

            Date dateTo = p.getSemester().getEndDate();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateFrom);

            int calendarWeekDay = DateUtils.getCalendarWeekDay(p.getWeekDay());
            // if pair date not set on weekend and first semester date set to weekend - move first
            // study date to the next business date
            if(!(calendarWeekDay==Calendar.SUNDAY || calendarWeekDay==Calendar.SATURDAY) && !DateUtils.isBusinessDay(calendar)){
                calendar = DateUtils.nextBusinessDay(calendar);
            }
            calendar.set(Calendar.DAY_OF_WEEK, calendarWeekDay);
            int weekNum =0;
            while(!calendar.getTime().after(dateTo)){
                weekNum++;
                if(calendar.getTime().before(dateFrom)){
                    calendar.add(Calendar.DAY_OF_WEEK,7);
                    continue;
                }
                if(p.getParity().equals(0) || (p.getParity().equals(1) && weekNum%2!=0) || (p.getParity().equals(2) && weekNum%2==0)){
                    pairDates.add(calendar.getTime());
                }
                calendar.add(Calendar.DAY_OF_WEEK,7);
            }
        }
        return pairDates;
    }
}

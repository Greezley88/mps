package od.onpu.mps.services.impl;

import od.onpu.mps.dao.GenericDao;
import od.onpu.mps.dao.UserRoleDao;
import od.onpu.mps.entity.UserRole;
import od.onpu.mps.services.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.management.Query;

/**
 * Created with IntelliJ IDEA.
 * User: StasMenshykov
 * Date: 5/18/13
 * Time: 3:20 PM
 * To change this template use File | Settings | File Templates.
 */
@Service("userRoleService")
public class UserRoleServiceImpl extends GenericServiceImpl<UserRole> implements UserRoleService {
    @Autowired
    UserRoleDao userRoleDao;

    @Autowired
    private UserRoleDao studentDao;

    @Override
    public GenericDao<UserRole> getDao() {
        return studentDao;
    }

    @Override
    public UserRole getRoleByRoleName(String roleName){

        return userRoleDao.getRole(roleName);
    }

}

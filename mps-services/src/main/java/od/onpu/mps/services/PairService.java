/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.services;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import od.onpu.mps.entity.Pair;

/**
 *
 * @author maxim
 */
public interface PairService extends GenericService<Pair> {

    Map<Integer, List<Pair>> getSchedule(Integer semesterId, Integer groupId);

    List<Pair> getPairsForStudySheet(Integer semester, Integer teacher, Integer lesson, Integer group);

    /**
     *  Generating working days for pair
     * @param pair
     * @return
     */
    Set<Date> getDatesForPair(Pair pair);

    List<Pair> getPairsForGroup(Integer semesterId,Integer groupId);
}

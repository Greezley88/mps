package od.onpu.mps.services.impl;

import od.onpu.mps.dao.GenericDao;
import od.onpu.mps.entity.DomainObject;
import od.onpu.mps.services.GenericService;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 07.05.13
 * Time: 18:48
 * To change this template use File | Settings | File Templates.
 */
public abstract class GenericServiceImpl<T extends DomainObject> implements GenericService<T> {


    @Override
    public void save(T entity) {
        getDao().save(entity);
    }

    @Override
    public T update(T entity) {
        return getDao().update(entity);
    }

    @Override
    public void delete(T entity) {
        getDao().delete(entity);
    }

    @Override
    public T getById(Integer id) {
        return getDao().getById(id);
    }

    @Override
    public List<T> getAll() {
        return getDao().getAll();
    }


    public abstract GenericDao<T> getDao();
}

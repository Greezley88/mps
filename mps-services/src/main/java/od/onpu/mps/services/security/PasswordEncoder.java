package od.onpu.mps.services.security;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: StasMenshykov
 * Date: 5/30/13
 * Time: 3:27 PM
 * To change this template use File | Settings | File Templates.
 */
@Service("passwordEncoder")
public class PasswordEncoder implements org.springframework.security.authentication.encoding.PasswordEncoder {

    public static final int SALT_LENGTH = 16;
    public static final int SALT_PLACE = 30;

    public String getHashedString(String password) {

        StringBuffer hexString = new StringBuffer();

        try{

        MessageDigest md = MessageDigest.getInstance("SHA-256");

        md.update(password.getBytes());

        byte byteData[] = md.digest();

        for (int i=0;i<byteData.length;i++) {
            String hex=Integer.toHexString(0xff & byteData[i]);
            if(hex.length()==1) hexString.append('0');
            hexString.append(hex);
        }
        }
        catch(NoSuchAlgorithmException ex) {}

        return hexString.toString();
    }

    public static String generateString(String characters, int length){
        Random rnd = new Random();

        char[] text = new char[length];

        for (int i = 0; i < length; i++)
        {
            text[i] = characters.charAt(rnd.nextInt(characters.length()));
        }
        return new String(text);
    }

    @Override
    public String encodePassword(String rawPass, Object salt) throws DataAccessException {
        try {
            String saltStr = randomSalt();
            return encrypt(rawPass, saltStr);
        } catch (Exception e) {
            throw new DataAccessResourceFailureException("Failed to encode password.", e);
        }
    }

    @Override
    public boolean isPasswordValid(String encPass, String rawPass, Object salt) throws DataAccessException {
        try {
            String saltStr = extractSaltFromHash(encPass);
            return encrypt(rawPass, saltStr).equals(encPass);
        } catch (Exception e) {
            throw new DataAccessResourceFailureException("Failed to validate password.", e);
        }
    }

    public String encrypt(String pass, String salt) {

        String saltPassStr = addSaltToPassword(pass, salt);
        String hash = getHashedString(saltPassStr);
        return addSaltToHash(hash, salt);
    }



    public String randomSalt() {
        return generateString("abcdef0123456789",SALT_LENGTH);
    }

    public String addSaltToPassword(String password, String salt) {

        return new StringBuilder(password).append(salt).toString();

    }

    public String addSaltToHash(String hash, String salt) {

        String part1 = hash.substring(0,SALT_PLACE);

        String part2 = hash.substring(SALT_PLACE);

        return new StringBuilder(part1).append(salt).append(part2).toString();
    }

    public String extractSaltFromHash(String hash) {

        return hash.substring(SALT_PLACE, SALT_PLACE + SALT_LENGTH);

    }


}

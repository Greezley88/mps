package od.onpu.mps.services;

import od.onpu.mps.dto.StudentRatingDto;
import od.onpu.mps.entity.Pair;
import od.onpu.mps.entity.Rating;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 14.05.13
 * Time: 9:57
 * To change this template use File | Settings | File Templates.
 */
public interface RatingService extends GenericService<Rating> {
    List<Rating> generateRatingList(Pair pair);

    List<StudentRatingDto> getStudentsRatingForPair(Integer groupId, Integer pairId);

    Integer updateRatingType(Integer[] modular, Integer finalRating, Integer pairId);

    List<StudentRatingDto> getFinalRatingForGroup(Integer groupId, Integer semesterId);

    Integer updateRating(Integer ratingId, Integer rating);
}

package od.onpu.mps.services;

import od.onpu.mps.entity.*;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: StasMenshykov
 * Date: 5/18/13
 * Time: 3:16 PM
 * To change this template use File | Settings | File Templates.
 */
public interface UserService extends UserDetailsService {

    public Boolean isLoginUnique(String login);

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;

    public User getUserById(Integer id);

    public User getCurrentUser();

    public List<User> getAll();

    public User update(User user);

    public Boolean isUserExists(Integer id, User.Status status);

    public void delete(User user);

    public List<User> getUserByLogin(String login);

    public List<User> getUsersBySurname(String surname);

    public List<Group> getGroups();

}

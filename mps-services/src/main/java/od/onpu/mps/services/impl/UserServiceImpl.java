package od.onpu.mps.services.impl;

import od.onpu.mps.dao.GenericDao;
import od.onpu.mps.dao.GroupDao;
import od.onpu.mps.dao.UserDao;
import od.onpu.mps.entity.Group;
import od.onpu.mps.entity.User;
import od.onpu.mps.services.UserService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: StasMenshykov
 * Date: 5/18/13
 * Time: 3:23 PM
 * To change this template use File | Settings | File Templates.
 */
@Service("userService")
public class UserServiceImpl extends GenericServiceImpl<User> implements UserService {

    @Autowired
    UserDao userDao;

    @Autowired
    GroupDao groupDao;

    @Autowired
    GroupServiceImpl groupService;



    @Override
    public GenericDao<User> getDao() {
        return userDao;
    }


    @Override
    @Transactional
    public Boolean isLoginUnique(String login)
    {
        String query = String.format("select obj from User obj where obj.login = '%s'", login);
        return userDao.executeQuery(query).size() == 0;
    }






    @Override
    @Transactional
    public UserDetails loadUserByUsername(String login)
    {
        if(login == null)
            throw new IllegalArgumentException("login parameter cannot be null.");
        User user = userDao.getUserByLogin(login);
      return  new org.springframework.security.core.userdetails.User(user.getUsername(),user.getPassword(),true,true,true,true,user.getAuthorities());

    }




    @Override
    public User getUserById(Integer id)
    {
        return userDao.getById(id);
    }



    @Override
    public User getCurrentUser(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if(principal.toString().equals("anonymousUser")) return  null;

        else {
           return userDao.getUserByLogin(((UserDetails)principal).getUsername());
        }

    }

    @Override
     public Boolean isUserExists(Integer id, User.Status status){
         if (User.Status.TEACHER == status) {
             String query = String.format("select obj from User obj where obj.teacher=%s",id);
             return userDao.executeQuery(query).size() != 0;
         }
         else if(User.Status.STUDENT == status) {
             String query = String.format("select obj from User obj where obj.student=%s",id);
             return userDao.executeQuery(query).size() != 0;
         }
        else return false;
     }

    @Override
    public List<User> getUserByLogin(String login){
        String query = String.format("select u from User u where u.login = '%s'",login);
        return userDao.executeQuery(query);
    }

    @Override
    public List<User> getUsersBySurname(String surname){
        String query1 = String.format("select u from User u where u.student.surname = '%s'",surname);
        String query2 = String.format("select u from User u where u.teacher.surname = '%s'",surname);
        List<User> list1 = userDao.executeQuery(query1);
        list1.addAll(userDao.executeQuery(query2));
        return list1;
    }

    @Override
    public List<Group> getGroups(){
       User user = getCurrentUser();
       Boolean flags[] = user.getRoleFlags();
        ArrayList<Group> group = new ArrayList<Group>();
       if(flags[0]) return groupService.getAll();
       if(flags[2]) return getTeacherGroups(user.getTeacher().getId());
       if(flags[3]) {
         group.add(user.getStudent().getGroup());
         return group;
       }
        return group;
    }

    public List<Group> getTeacherGroups(Integer id){
        String query = String.format("select distinct p.group from Pair p where p.teacher.id = '%s'",id);
        return groupDao.executeQuery(query);
    }





}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.services;

import java.util.List;
import od.onpu.mps.entity.Lesson;

/**
 *
 * @author maxim
 */
public interface LessonService extends GenericService<Lesson>{
    List<Lesson> getLessonsForGroup(Integer groupId,Integer semesterId);
}

package od.onpu.mps.services;

import od.onpu.mps.entity.UserRole;

/**
 * Created with IntelliJ IDEA.
 * User: StasMenshykov
 * Date: 5/18/13
 * Time: 3:17 PM
 * To change this template use File | Settings | File Templates.
 */
public interface UserRoleService extends GenericService<UserRole> {

     //   public UserRole getUserRoleByUserId(Long id);

  //  public void updateUserRole(UserRole userRole);

    public UserRole getRoleByRoleName(String roleName);
}


package od.onpu.mps.util;

import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 13.05.13
 * Time: 20:53
 * To change this template use File | Settings | File Templates.
 */
public class DateUtils {

    public static Calendar nextBusinessDay(Calendar calendar) {
        Calendar loc = (Calendar) calendar.clone();
        while (!isBusinessDay(calendar)){
            calendar.add(Calendar.DATE,1);
        }
        return calendar;
    }

    /**
     * we use weekDay from Monday to Sunday as 1 to 7
     * but java.util.Calendar use weekDays from Sunday to Saturday as 1 to 7 (Monday = 2)
     * this method is convert our weekDay to Calendar weekDay
     *
     * @param myWeekOfDay our weekDay
     * @return int Calendar weekDay
     */
    public static int getCalendarWeekDay(int myWeekOfDay){

        if(myWeekOfDay==7) return 1;

        return myWeekOfDay+1;
    }

    public static boolean isBusinessDay(Calendar calendar){

        boolean isSunday = calendar.get(Calendar.DAY_OF_WEEK)==Calendar.SUNDAY;
        boolean isSaturday = calendar.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY;

        return !(isSunday || isSaturday);
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.services.impl;

import static org.junit.Assert.*;

import od.onpu.mps.entity.Group;
import od.onpu.mps.entity.Lesson;
import od.onpu.mps.entity.Pair;
import od.onpu.mps.entity.Teacher;
import od.onpu.mps.services.GroupService;
import od.onpu.mps.services.LessonService;
import od.onpu.mps.services.PairService;
import od.onpu.mps.services.TeacherService;
import od.onpu.mps.util.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 *
 * @author maxim
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/META-INF/spring/spring-services.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class PairServiceImplTest {
    
    @Autowired
    private TeacherService teacherService;
    
    @Autowired
    private GroupService groupService;
    
    @Autowired
    private LessonService lessonService;
    
    @Autowired
    private PairService pairService;
    
    @Test
    public void testSavePair(){
        Teacher teacher = new Teacher();
        teacher.setName("Test1");
        teacher.setPatronymic("Test1");
        teacher.setSurname("Test1");
        teacher.setRank("Test1");
        teacher = teacherService.update(teacher);
        
        Group group = new Group();
        group.setCurator(teacher);
        group.setName("Name of Group");
        group = groupService.update(group);
        
        Lesson lesson = new Lesson();
        lesson.setName("Lesson");
        lesson = lessonService.update(lesson);
        
        Pair pair = new Pair();
        pair.setGroup(group);
        pair.setLesson(lesson);
        pair.setNumber(12);
        pair.setTeacher(teacher);
        
        pairService.save(pair);
        
        int count = pairService.getAll().size();
        assertFalse(count == 0);
    }
    
    @Test
    public void testGetByIdAndUpdate(){
        Teacher teacher = new Teacher();
        teacher.setName("Test1");
        teacher.setPatronymic("Test1");
        teacher.setSurname("Test1");
        teacher.setRank("Test1");
        teacher = teacherService.update(teacher);
        
        Group group = new Group();
        group.setCurator(teacher);
        group.setName("Name of Group");
        group = groupService.update(group);
        
        Lesson lesson = new Lesson();
        lesson.setName("Lesson");
        lesson = lessonService.update(lesson);
        
        Pair pair = new Pair();
        pair.setGroup(group);
        pair.setLesson(lesson);
        pair.setNumber(12);
        pair.setTeacher(teacher);
        
        pair = pairService.update(pair);
        
        Pair newPair = pairService.getById(pair.getId());

        assertTrue(pair.getNumber().equals(newPair.getNumber()));
    }

}

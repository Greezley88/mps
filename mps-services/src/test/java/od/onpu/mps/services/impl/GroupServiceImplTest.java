/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.services.impl;

import static org.junit.Assert.*;

import od.onpu.mps.entity.Group;
import od.onpu.mps.entity.Teacher;
import od.onpu.mps.services.GroupService;
import od.onpu.mps.services.TeacherService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author maxim
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/META-INF/spring/spring-services.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class GroupServiceImplTest {
    
    @Autowired
    private TeacherService teacherService;
    
    @Autowired
    private GroupService groupService;
    
    @Test
    public void testSaveGroup(){
        Teacher teacher = new Teacher();
        teacher.setName("Test1");
        teacher.setPatronymic("Test1");
        teacher.setSurname("Test1");
        teacher.setRank("Test1");
        
        teacher = teacherService.update(teacher);
        
        Group group = new Group();
        group.setCurator(teacher);
        group.setName("Name of Group");
        groupService.save(group);
        
        int count = groupService.getAll().size();
        assertFalse(count == 0);
    }
    
    @Test
    public void testGetByIdAndUpdateGroup(){
        Teacher teacher = new Teacher();
        teacher.setName("Test1");
        teacher.setPatronymic("Test1");
        teacher.setSurname("Test1");
        teacher.setRank("Test1");
        
        teacher = teacherService.update(teacher);
        
        Group group = new Group();
        group.setCurator(teacher);
        group.setName("Name of Group");
        
        group = groupService.update(group);
        
        Group newGroup = groupService.getById(group.getId());
        assertTrue(group.getName().equals(newGroup.getName()));
    }
    
}

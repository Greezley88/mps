/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.services.impl;

import od.onpu.mps.entity.Lesson;
import static org.junit.Assert.*;

import od.onpu.mps.services.LessonService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author maxim
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/META-INF/spring/spring-services.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class LessonServiceImplTest {
    
    @Autowired
    private LessonService lessonService;
    
    @Test
    public void testSaveLesson(){
        Lesson lesson = new Lesson();
        lesson.setName("Lesson");
        
        lessonService.save(lesson);
        
        int count = lessonService.getAll().size();
        assertFalse(count == 0);
    }
    
    @Test
    public void testGetByIdAndUpdate(){
        Lesson lesson = new Lesson();
        lesson.setName("Lesson");
        lesson = lessonService.update(lesson);
        
        Lesson newLesson = lessonService.getById(lesson.getId());
        
        assertTrue(lesson.getName().equals(newLesson.getName()));
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.services.impl;

import static org.junit.Assert.*;

import od.onpu.mps.entity.Teacher;
import od.onpu.mps.services.TeacherService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author maxim
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/META-INF/spring/spring-services.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class TeacherServiceImplTest {
    
    @Autowired
    private TeacherService teacherService;
    
    @Test
    public void testSaveTeacher(){
        Teacher teacher = new Teacher();
        teacher.setName("Test1");
        teacher.setPatronymic("Test1");
        teacher.setSurname("Test1");
        teacher.setRank("Test1");
        
        teacherService.save(teacher);
        
        int count = teacherService.getAll().size();
        assertFalse(count == 0);
    }
    
    @Test
    public void testGetByIdAndUpdateTeaher(){
        Teacher teacher = new Teacher();
        teacher.setName("Test1");
        teacher.setPatronymic("Test1");
        teacher.setSurname("Test1");
        teacher.setRank("Test1");
        
        teacher = teacherService.update(teacher);
        Teacher newTeacher = teacherService.getById(teacher.getId());
        assertTrue(teacher.getPatronymic().equals(newTeacher.getPatronymic()));
    }
    
}

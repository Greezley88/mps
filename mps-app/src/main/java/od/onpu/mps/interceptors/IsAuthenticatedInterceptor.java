package od.onpu.mps.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import od.onpu.mps.entity.User;
import od.onpu.mps.entity.UserRole;
import od.onpu.mps.services.StudentService;
import od.onpu.mps.services.TeacherService;
import od.onpu.mps.services.UserService;
import od.onpu.mps.services.security.PasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: StasMenshykov
 * Date: 5/27/13
 * Time: 3:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class IsAuthenticatedInterceptor extends HandlerInterceptorAdapter {
   // private static final Logger logger = LoggerFactory.getLogger(IsAuthenticatedInterceptor.class);

    @Autowired
    UserService userService;

   // @Autowired
   // PasswordEncoder passwordEncoder;

    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler)
            throws Exception {

        return true;
    }

    //after the handler is executed
    public void postHandle(
            HttpServletRequest request, HttpServletResponse response,
            Object handler, ModelAndView modelAndView)
            throws Exception{
           Boolean isAdmin=false;
           Boolean showTable = false;
           User user = userService.getCurrentUser();

           request.setAttribute("userMain",user);

           if(user != null){

               isAdmin = user.isAdmin();
               if(user.getRoleFlags()[0] || user.getRoleFlags()[2] || user.getRoleFlags()[3]) {
                   showTable = true;
                   request.setAttribute("showTable",showTable);
               }
               request.setAttribute("userDetails",user.getUserDetails());

           }
           request.setAttribute("isAdmin",isAdmin);
    }
}

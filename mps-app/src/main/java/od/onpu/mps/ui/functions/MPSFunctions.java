package od.onpu.mps.ui.functions;

import od.onpu.mps.entity.RatingType;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 04.05.13
 * Time: 15:24
 * To change this template use File | Settings | File Templates.
 */
public final class MPSFunctions {


    private MPSFunctions(){} // hidden constructor
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");

    /**
     * set class 'active' for menu if actual pageName equals to expected
     * @param actual
     * @param expected
     * @return
     */
    public static String activeLink(String actual,String expected){

        if(actual.contains(expected)){
            return "class=\"active\"";
        }
        return null;
    }

    /**
     * format date to dd.mm.YYYY
     * @param date
     * @return  String
     */
    public static String formatDate(Date date){
        if(date == null){
            return "null";
        }
         return simpleDateFormat.format(date);
    }


    public static String selectedOption(RatingType actual, RatingType expected){
        if(actual.equals(expected)){
            return "selected=\"selected\"";
        }
        return null;
    }

    public static String ratingCssClass(RatingType type){

        if(type.equals(RatingType.MODULAR)){
            return "class=\"modular\"";
        }else if(type.equals(RatingType.FINAL)){
            return "class=\"finalRating\"";
        }
        return null;
    }
}

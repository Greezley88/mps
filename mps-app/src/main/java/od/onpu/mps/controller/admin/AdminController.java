package od.onpu.mps.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 26.04.13
 * Time: 11:33
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminController {

    // обрабатывает "/admin"
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView index(){


        return new ModelAndView("admin/index");

    }

}

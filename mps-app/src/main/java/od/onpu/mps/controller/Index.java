package od.onpu.mps.controller;

import od.onpu.mps.entity.Group;
import od.onpu.mps.services.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 4/2/13
 * Time: 4:32 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class Index {

    @RequestMapping({"/index"})
    public ModelAndView index(){
        ModelAndView modelAndView =  new ModelAndView("index");
        return modelAndView;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.controller.admin;

import od.onpu.mps.entity.Group;
import od.onpu.mps.entity.Student;
import od.onpu.mps.services.GroupService;
import od.onpu.mps.services.StudentService;
import od.onpu.mps.validators.admin.StudentValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collections;

/**
 *
 * @author maxim
 */
@Controller
public class StudentController extends AdminController{
    
    @Autowired
    private StudentService studentService;
    
    @Autowired
    private GroupService groupService;
    
    @Autowired
    private StudentValidator studentValidator;
    
    @RequestMapping(value = {"/student/all"}, method = RequestMethod.GET)
    public ModelAndView getAllStudents(){
        ModelAndView modelAndView = new ModelAndView("admin/studentlist");
        modelAndView.addObject("students", studentService.getAll());
        modelAndView.addObject("pageName","students");
        return modelAndView;
    }
    
    @RequestMapping(value = {"/student/list/{id}"}, method = RequestMethod.GET)
    public ModelAndView students(@PathVariable("id") Integer id){
        ModelAndView modelAndView = new ModelAndView();
        Group group = groupService.getById(id);
        if(group == null){
            modelAndView.setViewName("redirect:/admin/group/list");
            return modelAndView;
        } else {
            modelAndView.setViewName("admin/studentlist");
            modelAndView.addObject("pageName","students");
            modelAndView.addObject("students", studentService.getByGroupId(group.getId()));// group.getStudents());
            return modelAndView;
        }
    }
    
    @RequestMapping(value = {"/student/add"}, method = RequestMethod.GET)
    public ModelAndView index(){
        ModelAndView modelAndView = new ModelAndView("admin/addstudent");
        modelAndView.addObject("groups", groupService.getAll());
        modelAndView.addObject("editable", false);
        modelAndView.addObject("student", new Student());
        return modelAndView;
    }
    
    @RequestMapping(value = {"/student/add"}, method = RequestMethod.POST)
    public ModelAndView addStudent(@ModelAttribute("student") Student student, BindingResult bindingResult){
        ModelAndView modelAndView = new ModelAndView();
        studentValidator.validate(student, bindingResult);
        if(bindingResult.hasErrors()){
            modelAndView.setViewName("admin/addstudent");
            modelAndView.addObject("groups", groupService.getAll());
            if(student.getId() == null){
                modelAndView.addObject("editable", false);
            } else {
                modelAndView.addObject("editable", true);
            }
            modelAndView.addObject("student", student);
            return modelAndView;
        } else {
            Group group = groupService.getById(student.getGroup().getId());
            student.setGroup(group);
            studentService.update(student);
            modelAndView.setViewName("redirect:/admin/student/list/" + group.getId());
            return modelAndView;
        }
    }
    
    @RequestMapping(value = {"/student/edit/{id}"}, method = RequestMethod.GET)
    public ModelAndView editStudent(@PathVariable("id") Integer id){
        ModelAndView modelAndView = new ModelAndView("admin/addstudent");
        Student student = studentService.getById(id);
        if(student == null){
            modelAndView.addObject("groups", groupService.getAll());
            modelAndView.addObject("editable", false);
            modelAndView.addObject("student", new Student());
        } else {
            modelAndView.addObject("groups", groupService.getAll());
            modelAndView.addObject("editable", true);
            modelAndView.addObject("student", student);
        }
        return modelAndView;
    }
    
    @RequestMapping(value = {"/student/delete/{id}"}, method = RequestMethod.GET)
    public ModelAndView deleteStudent(@PathVariable("id") Integer id){
        ModelAndView modelAndView = new ModelAndView("redirect:/admin/group/list");
        Student student = studentService.getById(id);
        String message = "";
        try{
            if(student != null){
                studentService.delete(student);
            }
        }catch (Exception e){
            message += "Operation failed.";
        }
        modelAndView.addObject("groups", groupService.getAll());
        return modelAndView;
    }
    
}

package od.onpu.mps.controller;

import od.onpu.mps.entity.*;
import od.onpu.mps.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 06.05.13
 * Time: 12:45
 *
 * Controller for table page
 *
 */
@Controller
@RequestMapping("/table")
public class Table {


    @Autowired
    private GroupService groupService;

    @Autowired
    private StudentService studentService;
    @Autowired
    private RatingService ratingService;
    @Autowired
    private PairService pairService;
    @Autowired
    private LessonService lessonService;

    @Autowired
    private  UserService userService;

    // default action
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView index(){
        List<Group> groups = userService.getGroups();

        ModelAndView modelAndView =  new ModelAndView("table/index");
        modelAndView.addObject("groups",groups);
        if(!groups.isEmpty()){
            modelAndView.addObject("students",studentService.getByGroupId(groups.get(0).getId()));
            modelAndView.addObject("defaultGroup",groups.get(0).getId());
        }
        return modelAndView;
    }

    @RequestMapping(value="/pair", method = RequestMethod.GET)
    public ModelAndView pairStat(){
        ModelAndView view = new ModelAndView("table/pair");
        List<Group> groups = userService.getGroups();
        view.addObject("groups",groups);

        return view;
    }

    @RequestMapping(value="/final", method = RequestMethod.GET)
    public ModelAndView itog(){
        ModelAndView view = new ModelAndView("table/itog");

        List<Group> groups = userService.getGroups();
        view.addObject("groups",groups);

        return view;
    }

    @RequestMapping(value="/pairRating", method =RequestMethod.POST)
    public ModelAndView pair(@RequestParam Integer groupId,@RequestParam Integer pairId,@RequestParam(required = false) String editable){

        ModelAndView view = new ModelAndView("snippets/pairRating");
        if(editable!=null){
            view.setViewName("snippets/editablePairRating");
        }
        Pair pair = pairService.getById(pairId);
        if(pair!=null){
            view.addObject("pair",pair);
            view.addObject("students",ratingService.getStudentsRatingForPair(groupId,pairId));
        }

        User user = userService.getCurrentUser();

        if(user != null) view.addObject("turnOn",user.canEdit(pair.getTeacher().getId()));

        return view;
    }

    @RequestMapping(value="/finalRating", method = RequestMethod.POST)
    public ModelAndView finalRating(@RequestParam Integer groupId,@RequestParam Integer semesterId){

        ModelAndView view = new ModelAndView("snippets/finalRating");

        view.addObject("students",ratingService.getFinalRatingForGroup(groupId,semesterId));

        return view;
    }

    @RequestMapping(value="/groupSelected", method = RequestMethod.GET)
    public @ResponseBody List<Student> groupSelected(@RequestParam Integer groupId){
        return studentService.getByGroupId(groupId);
    }

    @RequestMapping(value="/pairList", method = RequestMethod.GET)
    public @ResponseBody List<Pair> pairList(@RequestParam Integer groupId,@RequestParam Integer semesterId){
        return pairService.getPairsForGroup(semesterId, groupId);
    }

    @RequestMapping(value="/saveSettings",method = RequestMethod.POST)
    public @ResponseBody Boolean saveSettings(@RequestParam Integer[] module,@RequestParam Integer finalRating,@RequestParam Integer pair){

        ratingService.updateRatingType(module,finalRating,pair);

        return Boolean.TRUE;
    }
    @RequestMapping(value="/saveRating",method = RequestMethod.POST)
    public @ResponseBody Integer saveRating(@RequestParam Integer ratingId,@RequestParam Integer rating){

        return ratingService.updateRating(ratingId,rating);

    }


}

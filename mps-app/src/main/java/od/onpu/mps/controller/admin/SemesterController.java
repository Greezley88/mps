/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.controller.admin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import od.onpu.mps.dto.SemesterDTO;
import od.onpu.mps.entity.Semester;
import od.onpu.mps.services.SemesterService;
import od.onpu.mps.validators.admin.SemesterValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author maxim
 */
@Controller
public class SemesterController extends AdminController{
    
    @Autowired
    private SemesterService semesterService;
    
    @Autowired
    private SemesterValidator semesterValidator;
    
    @RequestMapping(value = {"/semester/list"}, method = RequestMethod.GET)
    public ModelAndView semesters(){
        ModelAndView modelAndView = new ModelAndView("admin/semesterlist");
        modelAndView.addObject("semesters", semesterService.getAll());
        modelAndView.addObject("pageName","semesters");
        return modelAndView;
    }
    
    @RequestMapping(value = {"/semester/add"},method = RequestMethod.GET)
    public ModelAndView index(){
        ModelAndView modelAndView = new ModelAndView("admin/addsemester");
        modelAndView.addObject("editable", false);
        modelAndView.addObject("semester", new SemesterDTO());
        return modelAndView;
    }
    
    @RequestMapping(value = {"/semester/add"},method = RequestMethod.POST)
    public ModelAndView addSemester(@ModelAttribute("semester") SemesterDTO semesterDTO, BindingResult bindingResult) throws ParseException{
        semesterValidator.validate(semesterDTO, bindingResult);
        ModelAndView modelAndView = new ModelAndView();
        if(bindingResult.hasErrors()){
            modelAndView.setViewName("admin/addsemester");
            if(semesterDTO.getId() == null){
                modelAndView.addObject("editable", false);
            } else {
                modelAndView.addObject("editable", true);
            }
            modelAndView.addObject("semester", semesterDTO);
            return modelAndView;
        } else{
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Semester semester = new Semester();
            semester.setId(semesterDTO.getId());
            semester.setStartDate(simpleDateFormat.parse(semesterDTO.getStartDate()));
            semester.setEndDate(simpleDateFormat.parse(semesterDTO.getEndDate()));
            semesterService.update(semester);
            modelAndView.setViewName("redirect:/admin/semester/list");
            modelAndView.addObject("pageName","semesters");
            modelAndView.addObject("semesters", semesterService.getAll());
            return modelAndView;
        }
    }
    
    @RequestMapping(value = {"/semester/edit/{id}"}, method = RequestMethod.GET)
    public ModelAndView editSemester(@PathVariable("id") Integer id){
        if(id == null){
            ModelAndView modelAndView = new ModelAndView("admin/semesterlist");
            modelAndView.addObject("message", "Bad id");
            modelAndView.addObject("semesters", semesterService.getAll());
            return modelAndView;
        }
        ModelAndView  modelAndView = new ModelAndView("admin/addsemester");
        Semester semester = semesterService.getById(id);
        if(semester == null){
            modelAndView.addObject("semester", new SemesterDTO());
            modelAndView.addObject("editable", false);
        } else {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SemesterDTO semesterDTO = new SemesterDTO();
            semesterDTO.setId(semester.getId());
            semesterDTO.setStartDate(simpleDateFormat.format(semester.getStartDate()));
            semesterDTO.setEndDate(simpleDateFormat.format(semester.getEndDate()));
            modelAndView.addObject("semester", semesterDTO);
            modelAndView.addObject("editable", true);
        }
        return modelAndView;
    }
    
    @RequestMapping(value = {"/semester/delete/{id}"}, method = RequestMethod.GET)
    public ModelAndView deleteSemester(@PathVariable("id") Integer id){
        ModelAndView modelAndView = new ModelAndView("admin/semesterlist");
        if(id == null){
            modelAndView.addObject("message", "Bad id");
            modelAndView.addObject("semesters", semesterService.getAll());
            return modelAndView;
        }
        Semester semester = semesterService.getById(id);
        String message = "";
        try{
            if(semester != null){
                semesterService.delete(semester);
            }
        }catch (Exception e){
            message += "Operation failed.";
        }
        modelAndView.addObject("message", message);
        modelAndView.addObject("pageName","semesters");
        modelAndView.addObject("semesters", semesterService.getAll());
        return modelAndView;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.controller.admin;

import od.onpu.mps.entity.Teacher;
import od.onpu.mps.services.TeacherService;
import od.onpu.mps.validators.admin.TeacherValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author maxim
 */
@Controller
public class TeacherController extends AdminController{
    
    @Autowired
    private TeacherService teacherService;
    
    @Autowired
    private TeacherValidator teacherValidator;
    
    @RequestMapping(value = {"/teacher/add"},method = RequestMethod.GET)
    public ModelAndView index(){
        ModelAndView modelAndView = new ModelAndView("admin/addteacher");
        modelAndView.addObject("editable", false);
        modelAndView.addObject("teacher", new Teacher());
        return modelAndView;
    }
    
    @RequestMapping(value = {"/teacher/add"},method = RequestMethod.POST)
    public ModelAndView addTeacher(@ModelAttribute("teacher")Teacher teacher, BindingResult bindingResult){
        teacherValidator.validate(teacher, bindingResult);
        ModelAndView modelAndView = new ModelAndView();
        if(bindingResult.hasErrors()){
            modelAndView.setViewName("admin/addteacher");
            if(teacher.getId() == null){
                modelAndView.addObject("editable", false);
            } else {
                modelAndView.addObject("editable", true);
            }
            modelAndView.addObject("teacher", teacher);
            return modelAndView;
        } else{
            teacherService.update(teacher);
            modelAndView.setViewName("redirect:/admin/teacher/list");
            modelAndView.addObject("teachers", teacherService.getAll());
            return modelAndView;
        }
    }
    
    @RequestMapping(value = {"/teacher/edit/{id}"}, method = RequestMethod.GET)
    public ModelAndView editTeacher(@PathVariable("id") Integer id){
        if(id == null){
            ModelAndView modelAndView = new ModelAndView("admin/teacherlist");
            modelAndView.addObject("message", "Bag id");
            modelAndView.addObject("teachers", teacherService.getAll());
            return modelAndView;
        }
        ModelAndView  modelAndView = new ModelAndView("admin/addteacher");
        Teacher teacher = teacherService.getById(id);
        if(teacher == null){
            modelAndView.addObject("teacher", new Teacher());
            modelAndView.addObject("editable", false);
        } else {
            modelAndView.addObject("teacher", teacher);
            modelAndView.addObject("editable", true);
        }
        return modelAndView;
    }
    
    @RequestMapping(value = {"/teacher/delete/{id}"}, method = RequestMethod.GET)
    public ModelAndView deleteTeacher(@PathVariable("id") Integer id){
        ModelAndView modelAndView = new ModelAndView("admin/teacherlist");
        if(id == null){
            modelAndView.addObject("message", "Bag id");
            modelAndView.addObject("teachers", teacherService.getAll());
            return modelAndView;
        }
        Teacher teacher = teacherService.getById(id);
        String message = "";
        try{
            if(teacher != null){
                teacherService.delete(teacher);
            }
        }catch (Exception e){
            message += "Operation failed.";
        }
        modelAndView.addObject("message", message);
        modelAndView.addObject("teachers", teacherService.getAll());
        return modelAndView;
    }
    
    
    @RequestMapping(value = {"/teacher/list"}, method = RequestMethod.GET)
    public ModelAndView teachers(){
        ModelAndView modelAndView = new ModelAndView("admin/teacherlist");
        modelAndView.addObject("teachers", teacherService.getAll());
        modelAndView.addObject("pageName","teachers");
        return modelAndView;
    }
    
}

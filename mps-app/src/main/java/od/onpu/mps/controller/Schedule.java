package od.onpu.mps.controller;

import od.onpu.mps.entity.Group;
import od.onpu.mps.entity.Pair;
import od.onpu.mps.entity.Semester;
import od.onpu.mps.services.GroupService;
import od.onpu.mps.services.PairService;
import od.onpu.mps.services.SemesterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ppavlenko
 * Date: 06.05.13
 * Time: 21:29
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/schedule")
public class Schedule {

    @Autowired
    private GroupService groupService;

    @Autowired
    private SemesterService semesterService;

    @Autowired
    private PairService pairService;


    // default action
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView index(){
        List<Group> groups = groupService.getAll();

        ModelAndView modelAndView =  new ModelAndView("schedule");
        modelAndView.addObject("groups",groups);
        if(!groups.isEmpty()){
            List<Semester> semesters = semesterService.getAllForGroup(groups.get(0).getId());
            modelAndView.addObject("semesters",semesters);
            modelAndView.addObject("defaultGroup",groups.get(0).getId());
            modelAndView.addObject("schedule",pairService.getSchedule(semesters.isEmpty()?0:semesters.get(0).getId(),groups.get(0).getId()));
        }
        return modelAndView;
    }

    // ajax handler
    // function provides list of available semesters for group
    @RequestMapping(value="/groupSemesters", method = RequestMethod.GET)
    public @ResponseBody List<Semester> groupSemesters(@RequestParam Integer groupId){

        return semesterService.getAllForGroup(groupId);
    }


    @RequestMapping(value="/showSchedule", method = RequestMethod.GET)
    public ModelAndView showSchedule(@RequestParam Integer semesterId,@RequestParam Integer groupId){

        ModelAndView view = new ModelAndView("snippets/schedule-snippet");
        view.addObject("schedule",pairService.getSchedule(semesterId,groupId));

        return view;
    }
}

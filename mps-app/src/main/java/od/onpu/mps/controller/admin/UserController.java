package od.onpu.mps.controller.admin;

import od.onpu.mps.dto.EditUsersDTO;
import od.onpu.mps.dto.UsersDTO;
import od.onpu.mps.entity.Student;
import od.onpu.mps.entity.Teacher;
import od.onpu.mps.entity.User;
import od.onpu.mps.entity.UserRole;
import od.onpu.mps.services.*;
import od.onpu.mps.validators.admin.EditUserValidator;
import od.onpu.mps.validators.admin.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: StasMenshykov
 * Date: 5/28/13
 * Time: 4:27 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class UserController extends AdminController{

    @Autowired
    private UserService userService;
    @Autowired
    private TeacherService teacherService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private UserValidator userValidator;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private EditUserValidator editUserValidator;


    @RequestMapping(value = {"/user/list"}, method = RequestMethod.GET)
    public ModelAndView userList(){
        ModelAndView modelAndView = new ModelAndView("admin/userlist");
        modelAndView.addObject("users",new ArrayList<User>());
        modelAndView.addObject("pageName","users");
        return modelAndView;
    }

    @RequestMapping(value = {"/user/search"}, method = RequestMethod.POST)
    public ModelAndView userSearch(@RequestParam String name){
        ModelAndView modelAndView = new ModelAndView("admin/userlist");
        List<User> users = new ArrayList<User>();
        users = userService.getUsersBySurname(name);
        users.addAll(userService.getUserByLogin(name));
        modelAndView.addObject("pageName","users");
        return modelAndView.addObject("users", users);

    }


    @RequestMapping(value={"/user/groupSelected"}, method = RequestMethod.GET)
    public @ResponseBody List<Student> groupSelected(@RequestParam Integer groupId){

         return studentService.getNotUsersByGroupId(groupId);//group.getStudents();
    }

    @RequestMapping(value="/user/status", method = RequestMethod.GET)
    public  ModelAndView showView(@RequestParam Integer stat){

        ModelAndView view;

         if(stat == 2) {
         view = new ModelAndView("snippets/userTeacher");
        view.addObject("teachers", teacherService.getNotUsers());

             return view;
         }
        else if(stat ==1 )  {
         view = new ModelAndView("snippets/userGroups");
         view.addObject("groups", groupService.getAll());

             return view;
         }

         else return null;

    }



    @RequestMapping(value = {"/user/add"}, method = RequestMethod.GET)
    public ModelAndView index(){
        ModelAndView modelAndView = new ModelAndView("admin/addUser");
        modelAndView.addObject("usersDTO", new UsersDTO());
       // modelAndView.addObject("userRoles",userRoleService.getAll());
        return modelAndView;
    }

    @RequestMapping(value = {"/user/add"}, method = RequestMethod.POST)
    public ModelAndView addStudent(@ModelAttribute("usersDTO") UsersDTO usersDTO, BindingResult bindingResult, @RequestParam(required = false) Integer teacher, @RequestParam(required = false) Integer student, @RequestParam Integer status){
        List<UserRole> rolesList = new ArrayList<UserRole>();
        ModelAndView modelAndView = new ModelAndView();

        userValidator.validate(usersDTO, bindingResult);


        if(bindingResult.hasErrors()){

            modelAndView.setViewName("admin/addUser");
            usersDTO.setPassword(null);
            usersDTO.setPasswordCheck(null);
            modelAndView.addObject("usersDTO", usersDTO);
            //modelAndView.addObject("userRoles",userRoleService.getAll());
            return modelAndView;
        } else {
            User user = new User();
            user.setEnabled(1);
            String password = usersDTO.getPassword();
            password = passwordEncoder.encodePassword(password, null);
            user.setPassword(password);
            user.setLogin(usersDTO.getLogin());
            if(status != 3){
            if(teacher != null) {
                user.setTeacher(teacherService.getById(teacher));
                rolesList.add(userRoleService.getRoleByRoleName("ROLE_TEACHER"));
            }

            if (student != null) {
                user.setStudent(studentService.getById(student));
                rolesList.add(userRoleService.getRoleByRoleName("ROLE_STUDENT"));
            }

            }
            for (Integer role: usersDTO.getRoles()) rolesList.add(userRoleService.getById(role));
            user.setRole(rolesList);
            userService.update(user);
            modelAndView.setViewName("redirect:/admin/user/list");
            return modelAndView;
            }
        }

    @RequestMapping(value = {"/user/edit/{id}"}, method = RequestMethod.GET)
    public ModelAndView editUser(@PathVariable("id") Integer id){
        ModelAndView modelAndView = new ModelAndView();
         EditUsersDTO editUser = new EditUsersDTO();
         editUser.setId(id);
         modelAndView.setViewName("admin/changeUser");
            modelAndView.addObject("editUser",editUser);
        return modelAndView;
    }

    @RequestMapping(value = {"/user/edit/{id}"}, method = RequestMethod.POST)
    public ModelAndView saveUser(@ModelAttribute("editUser") EditUsersDTO editUser,@PathVariable("id") Integer id, BindingResult bindingResult){
        ModelAndView modelAndView = new ModelAndView();
    editUserValidator.validate(editUser,bindingResult);
        if(bindingResult.hasErrors()){
         editUser.setPassword(null);
         editUser.setPasswordCheck(null);
         modelAndView.setViewName("admin/changeUser");
         modelAndView.addObject("editUser", editUser);
            return modelAndView;
        }

        String password = editUser.getPassword();
        password = passwordEncoder.encodePassword(password, null);
        User user = userService.getUserById(id);
        user.setPassword(password);
        userService.update(user);
        modelAndView.setViewName("redirect:/admin/user/list");
        return modelAndView;
    }

    @RequestMapping(value = {"/user/delete/{id}"}, method = RequestMethod.GET)
    public ModelAndView deleteUser(@PathVariable("id") Integer id){
        ModelAndView modelAndView = new ModelAndView("redirect:/admin/user/list");
        User user = userService.getUserById(id);
        Integer currId = userService.getCurrentUser().getId();
        String message = "";
        try{
            if(user != null && id != currId ){
                userService.delete(user);
            }
        }catch (Exception e){
            message += "Operation failed.";
        }
        modelAndView.addObject("users", userService.getAll());

        return modelAndView;
    }

}

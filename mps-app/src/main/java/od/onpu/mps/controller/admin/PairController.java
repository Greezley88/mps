/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.controller.admin;

import java.util.List;
import java.util.Map;
import od.onpu.mps.entity.Group;
import od.onpu.mps.entity.Lesson;
import od.onpu.mps.entity.Pair;
import od.onpu.mps.entity.Semester;
import od.onpu.mps.entity.Teacher;
import od.onpu.mps.services.*;
import od.onpu.mps.services.SemesterService;
import od.onpu.mps.validators.admin.PairValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author maxim
 */
@Controller
public class PairController extends AdminController{
    
    @Autowired
    private TeacherService teacherService;
    
    @Autowired
    private GroupService groupService;
    
    @Autowired
    private LessonService lessonService;
    
    @Autowired
    private PairService pairService;

    @Autowired
    private RatingService ratingService;
    @Autowired
    private SemesterService semesterService;
    
    @Autowired
    private PairValidator pairValidator;
    
    @RequestMapping(value = {"/pair/all"}, method = RequestMethod.GET)
    public ModelAndView index(){
        ModelAndView modelAndView = new ModelAndView("admin/pairlist");
        modelAndView.addObject("pageName","pairs");
        modelAndView.addObject("pairs", pairService.getAll());
        return modelAndView;
    }
    
    @RequestMapping(value = {"/pair/list/{gid}"}, method = RequestMethod.GET)
    public ModelAndView pairsForGroup(@PathVariable("gid") Integer id){
        ModelAndView modelAndView = new ModelAndView();
        Group group = groupService.getById(id);
        if(group == null){
            modelAndView.setViewName("redirect:/admin/group/list");
            return modelAndView;
        } else {
            List semesters = semesterService.getAll();
            if(semesters.size() == 0){
                modelAndView.setViewName("redirect:/admin/semester/list");
                modelAndView.addObject("message", "Добавьте сначала семестры");
                modelAndView.addObject("pageName","semesters");
                return modelAndView;
            }else{
                modelAndView.setViewName("redirect:/admin/pair/list/" + id + "/"
                        + ((Semester)semesters.get(0)).getId());
                modelAndView.addObject("pageName","pairs");
            }
            return modelAndView;
        }
    }
    
    @RequestMapping(value = {"/pair/list/{gid}/{sid}"}, method = RequestMethod.GET)
    public ModelAndView pairs(@PathVariable("gid") Integer id, @PathVariable("sid") Integer sId){
        ModelAndView modelAndView = new ModelAndView();
        Group group = groupService.getById(id);
        Semester semester = semesterService.getById(sId);
        if(group == null || semester == null){
            modelAndView.setViewName("redirect:/admin/group/list");
            return modelAndView;
        } else {
            modelAndView.setViewName("admin/pairlist");
            modelAndView.addObject("pageName","pairs");
            modelAndView.addObject("groupId", id);
            modelAndView.addObject("semesterId", sId);
            modelAndView.addObject("semesters", semesterService.getAll());
            modelAndView.addObject("groupName", group.getName());
            Map pairs = pairService.getSchedule(sId, id);
            List mon = (List) pairs.get(1);
            List tue = (List) pairs.get(2);
            List wed = (List) pairs.get(3);
            List thu = (List) pairs.get(4);
            List fri = (List) pairs.get(5);
            modelAndView.addObject("mon", mon);
            modelAndView.addObject("tue", tue);
            modelAndView.addObject("wed", wed);
            modelAndView.addObject("thu", thu);
            modelAndView.addObject("fri", fri);
            return modelAndView;
        }
    }
    
    @RequestMapping(value = {"/pair/selectsemester"}, method = RequestMethod.POST)
    public ModelAndView selectSemester(@RequestParam("group") String group,
        @RequestParam("semester") String semester){
        if(group == null || semester == null){
            ModelAndView modelAndView = new ModelAndView("redirect:/admin/group/list");
            return modelAndView;
        }
        int gId = Integer.parseInt(group);
        int sId = Integer.parseInt(semester);
        ModelAndView modelAndView = new ModelAndView("redirect:/admin/pair/list/"
                + gId + "/" + sId);
        return modelAndView;
    }
    
    @RequestMapping(value = {"/pair/add/{gid}/{sid}"}, method = RequestMethod.GET)
    public ModelAndView add(@PathVariable("gid") Integer id, @PathVariable("sid") Integer sId){
        Group group = groupService.getById(id);
        Semester semester = semesterService.getById(sId);
        if(group == null || semester == null){
            ModelAndView modelAndView = new ModelAndView("redirect:/admin/group/list");
            return modelAndView;
        } else {
            ModelAndView modelAndView = new ModelAndView("admin/addpair");
            modelAndView.addObject("lessons", lessonService.getAll());
            modelAndView.addObject("teachers", teacherService.getAll());
            modelAndView.addObject("groups", groupService.getAll());
            modelAndView.addObject("semesters", semesterService.getAll());
            modelAndView.addObject("editable", false);
            Pair pair = new Pair();
            pair.setGroup(group);
            pair.setSemester(semester);
            modelAndView.addObject("pair", pair);
            return modelAndView;
        }
    }
    
    @RequestMapping(value = {"/pair/add"}, method = RequestMethod.POST)
    public ModelAndView addPair(@ModelAttribute("pair") Pair pair, BindingResult bindingResult){
        ModelAndView modelAndView = new ModelAndView();
        pairValidator.validate(pair, bindingResult);
        if(bindingResult.hasErrors()){
            modelAndView.setViewName("admin/addpair");
            modelAndView.addObject("lessons", lessonService.getAll());
            modelAndView.addObject("teachers", teacherService.getAll());
            modelAndView.addObject("groups", groupService.getAll());
            modelAndView.addObject("semesters", semesterService.getAll());
            if(pair.getId() == null){
                modelAndView.addObject("editable", false);
            } else {
                modelAndView.addObject("editable", true);
            }
            modelAndView.addObject("pair", pair);
            return modelAndView;
        } else {
            Lesson lesson = lessonService.getById(pair.getLesson().getId());
            pair.setLesson(lesson);
            Teacher teacher = teacherService.getById(pair.getTeacher().getId());
            pair.setTeacher(teacher);
            Group group = groupService.getById(pair.getGroup().getId());
            pair.setGroup(group);
            Semester semester = semesterService.getById(pair.getSemester().getId());
            pair.setSemester(semester);
            
            pairService.save(pair);
            
            // after saving pair - generate rating list for this pair
            ratingService.generateRatingList(pair);
            
            int gId = pair.getGroup().getId();
            int sId = pair.getSemester().getId();
            modelAndView.setViewName("redirect:/admin/pair/list/"
                    + gId + "/" + sId);
            
            modelAndView.addObject("pairs", pairService.getAll());
            return modelAndView;
        }
    }
    
    @RequestMapping(value = {"/pair/delete/{id}"}, method = RequestMethod.GET)
    public ModelAndView deletePair(@PathVariable("id") Integer id){
        if(id == null){
            ModelAndView modelAndView = new ModelAndView("redirect:/admin/group/list");
            return modelAndView;
        }
        Pair pair = pairService.getById(id);
        int gId = pair.getGroup().getId();
        int sId = pair.getSemester().getId();
        ModelAndView modelAndView = new ModelAndView("redirect:/admin/pair/list/"
                + gId + "/" + sId);
        try{
            if(pair != null){
                pairService.delete(pair);
            }
        }catch (Exception e){
            ModelAndView modelAndView2 = new ModelAndView("redirect:/admin/group/list");
            return modelAndView2;
        }
        return modelAndView;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.controller.admin;

import od.onpu.mps.entity.Group;
import od.onpu.mps.entity.Teacher;
import od.onpu.mps.services.GroupService;
import od.onpu.mps.services.TeacherService;
import od.onpu.mps.validators.admin.GroupValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author maxim
 */
@Controller
public class GroupController extends AdminController {
    
    @Autowired
    private TeacherService teacherService;
    
    @Autowired
    private GroupService groupService;
    
    @Autowired
    private GroupValidator groupValidator;
    
    @RequestMapping(value = {"/group/list"}, method = RequestMethod.GET)
    public ModelAndView groups(){
        ModelAndView modelAndView = new ModelAndView("admin/grouplist");
        modelAndView.addObject("groups", groupService.getAll());
        modelAndView.addObject("pageName","groups");
        return modelAndView;
    }
    
    @RequestMapping(value = {"/group/add"}, method = RequestMethod.GET)
    public ModelAndView index(){
        ModelAndView modelAndView = new ModelAndView("admin/addgroup");
        modelAndView.addObject("teachers", teacherService.getAll());
        modelAndView.addObject("editable", false);
        modelAndView.addObject("group", new Group());
        return modelAndView;
    }
    
    @RequestMapping(value = {"/group/add"}, method = RequestMethod.POST)
    public ModelAndView addGroup(@ModelAttribute("group") Group group, BindingResult bindingResult){
        ModelAndView modelAndView = new ModelAndView();
        groupValidator.validate(group, bindingResult);
        if(bindingResult.hasErrors()){
            modelAndView.setViewName("admin/addgroup");
            modelAndView.addObject("teachers", teacherService.getAll());
            if(group.getId() == null){
                modelAndView.addObject("editable", false);
            } else {
                modelAndView.addObject("editable", true);
            }
            modelAndView.addObject("group", group);
            return modelAndView;
        } else {
            Teacher cur = teacherService.getById(group.getCurator().getId());
            group.setCurator(cur);
            groupService.update(group);
            modelAndView.addObject("groups", groupService.getAll());
            modelAndView.setViewName("redirect:/admin/group/list");
            return modelAndView;
        }
    }
    
    @RequestMapping(value = {"/group/edit/{id}"}, method = RequestMethod.GET)
    public ModelAndView editGroup(@PathVariable("id") Integer id){
        if(id == null){
            ModelAndView modelAndView = new ModelAndView("admin/grouplist");
            modelAndView.addObject("message", "Bad id");
            modelAndView.addObject("groups", groupService.getAll());
            return modelAndView;
        }
        ModelAndView modelAndView = new ModelAndView("admin/addgroup");
        Group group = groupService.getById(id);
        if(group == null){
            modelAndView.addObject("teachers", teacherService.getAll());
            modelAndView.addObject("editable", false);
            modelAndView.addObject("group", new Group());
        } else {
            modelAndView.addObject("teachers", teacherService.getAll());
            modelAndView.addObject("editable", true);
            modelAndView.addObject("group", group);
        }
        return modelAndView;
    }
    
    @RequestMapping(value = {"/group/delete/{id}"}, method = RequestMethod.GET)
    public ModelAndView deleteGroup(@PathVariable("id") Integer id){
        ModelAndView modelAndView = new ModelAndView("admin/grouplist");
        if(id == null){
            modelAndView.addObject("message", "Bad id");
            modelAndView.addObject("groups", groupService.getAll());
            return modelAndView;
        }
        Group group = groupService.getById(id);
        String message = "";
        try{
            if(group != null){
                groupService.delete(group);
            }
        }catch (Exception e){
            message += "Operation failed.";
        }
        modelAndView.addObject("message", message);
        modelAndView.addObject("groups", groupService.getAll());
        return modelAndView;
    }



}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.controller.admin;

import od.onpu.mps.entity.Lesson;
import od.onpu.mps.services.LessonService;
import od.onpu.mps.validators.admin.LessonValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author maxim
 */
@Controller
public class LessonController extends AdminController{
    
    @Autowired
    private LessonService lessonService;
    
    @Autowired
    private LessonValidator lessonValidator;
    
    @RequestMapping(value = {"/lesson/add"}, method = RequestMethod.GET)
    public ModelAndView index(){
        ModelAndView modelAndView = new ModelAndView("admin/addlesson");
        modelAndView.addObject("editable", false);
        modelAndView.addObject("lesson", new Lesson());
        return modelAndView;
    }
    
    @RequestMapping(value = {"/lesson/add"}, method = RequestMethod.POST)
    public ModelAndView addLesson(@ModelAttribute("lesson") Lesson lesson, BindingResult bindingResult){
        lessonValidator.validate(lesson, bindingResult);
        ModelAndView modelAndView = new ModelAndView();
        if(bindingResult.hasErrors()){
            modelAndView.setViewName("admin/addlesson");
            if(lesson.getId() == null){
                modelAndView.addObject("editable", false);
            } else {
                modelAndView.addObject("editable", true);
            }
            modelAndView.addObject("lesson", lesson);
            return modelAndView;
        } else {
            lessonService.update(lesson);
            modelAndView.setViewName("redirect:/admin/lesson/list");
            modelAndView.addObject("lessons", lessonService.getAll());
            return modelAndView;
        }
    }
    
    @RequestMapping(value = {"/lesson/edit/{id}"}, method = RequestMethod.GET)
    public ModelAndView editLesson(@PathVariable("id") Integer id){
        if(id == null){
            ModelAndView modelAndView = new ModelAndView("admin/lessonlist");
            modelAndView.addObject("message", "Bag id");
            modelAndView.addObject("lessons", lessonService.getAll());
            return modelAndView;
        }
        ModelAndView modelAndView = new ModelAndView("admin/addlesson");
        Lesson lesson = lessonService.getById(id);
        if(lesson == null){
            modelAndView.addObject("lesson", new Lesson());
            modelAndView.addObject("editable", false);
        } else {
            modelAndView.addObject("lesson", lesson);
            modelAndView.addObject("editable", true);
        }
        return modelAndView;
    }
    
    @RequestMapping(value = {"/lesson/delete/{id}"}, method = RequestMethod.GET)
    public ModelAndView deleteLesson(@PathVariable("id") Integer id){
        ModelAndView modelAndView = new ModelAndView("admin/lessonlist");
        if(id == null){
            modelAndView.addObject("message", "Bag id");
            modelAndView.addObject("lessons", lessonService.getAll());
            return modelAndView;
        }
        Lesson lesson = lessonService.getById(id);
        String message = "";
        try{
            if(lesson != null){
                lessonService.delete(lesson);
            }
        }catch (Exception e){
            message += "Operation failed.";
        }
        modelAndView.addObject("message", message);
        modelAndView.addObject("lessons", lessonService.getAll());
        return modelAndView;
    }
    
    @RequestMapping(value = {"/lesson/list"}, method = RequestMethod.GET)
    public ModelAndView lessons(){
        ModelAndView modelAndView = new ModelAndView("admin/lessonlist");
        modelAndView.addObject("lessons", lessonService.getAll());
        modelAndView.addObject("pageName","lessons");
        return modelAndView;
    }
    
}

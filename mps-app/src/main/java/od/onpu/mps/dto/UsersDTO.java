package od.onpu.mps.dto;

/**
 * Created with IntelliJ IDEA.
 * User: Станислав
 * Date: 6/8/13
 * Time: 7:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class UsersDTO {

    private String login;

    private String password;

    private String passwordCheck;

    private Integer roles[];

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public Integer[] getRoles() {
        return roles;
    }

    public void setRoles(Integer[] roles) {
        this.roles = roles;
    }


    public String getPasswordCheck() {
        return passwordCheck;
    }

    public void setPasswordCheck(String passwordCheck) {
        this.passwordCheck = passwordCheck;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.dto;

/**
 *
 * @author maxim
 */
public class SemesterDTO {
    
    private Integer id;

    private String startDate;

    private String endDate;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

}

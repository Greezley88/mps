/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.dto;

import java.util.List;
import od.onpu.mps.entity.Pair;

/**
 *
 * @author maxim
 */
public class PairsDTO {
    
    private List<Pair> pairs;

    public List<Pair> getPairs() {
        return pairs;
    }

    public void setPairs(List<Pair> pairs) {
        this.pairs = pairs;
    }
    
}

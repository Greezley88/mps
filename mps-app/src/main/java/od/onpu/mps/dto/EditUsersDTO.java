package od.onpu.mps.dto;

/**
 * Created with IntelliJ IDEA.
 * User: Станислав
 * Date: 6/9/13
 * Time: 3:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class EditUsersDTO {

    Integer id;

    String password;

    String passwordCheck;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordCheck() {
        return passwordCheck;
    }

    public void setPasswordCheck(String passwordCheck) {
        this.passwordCheck = passwordCheck;
    }
}

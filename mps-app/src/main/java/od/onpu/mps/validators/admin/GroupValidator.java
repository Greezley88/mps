/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.validators.admin;

import od.onpu.mps.entity.Group;
import od.onpu.mps.entity.Teacher;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author maxim
 */
public class GroupValidator implements Validator{

    @Override
    public boolean supports(Class<?> type) {
        return Group.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "name", "Name must be not empty");
        Teacher curator = ((Group) o).getCurator();
        if(curator.getId() == -1){
            errors.rejectValue("curator", "curator", "Please select curator");
        }
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.validators.admin;

import od.onpu.mps.dto.SemesterDTO;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author maxim
 */
public class SemesterValidator implements Validator{

    @Override
    public boolean supports(Class<?> type) {
        return SemesterDTO.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "startDate", "startDate", "Start date must be not empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "endDate", "endDate", "End date must be not empty");
    }
    
}

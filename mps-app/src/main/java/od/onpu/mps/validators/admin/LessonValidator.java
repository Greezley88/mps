/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.validators.admin;

import od.onpu.mps.entity.Lesson;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author maxim
 */
public class LessonValidator implements Validator{

    @Override
    public boolean supports(Class<?> type) {
        return Lesson.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "name", "Name must be not empty");
    }
    
}

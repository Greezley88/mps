/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.validators.admin;

import od.onpu.mps.entity.Group;
import od.onpu.mps.entity.Lesson;
import od.onpu.mps.entity.Pair;
import od.onpu.mps.entity.Semester;
import od.onpu.mps.entity.Teacher;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author maxim
 */
public class PairValidator implements Validator{

    @Override
    public boolean supports(Class<?> type) {
        return Pair.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "weekDay", "weekDay", "Week Day must be not empty");
        Lesson lesson = ((Pair) o).getLesson();
        if(lesson.getId() == -1){
            errors.rejectValue("lesson", "lesson", "Please select lesson");
        }
        Teacher teacher = ((Pair) o).getTeacher();
        if(teacher.getId() == -1){
            errors.rejectValue("teacher", "teacher", "Please select teacher");
        }
        Group group = ((Pair) o).getGroup();
        if(group.getId() == -1){
            errors.rejectValue("group", "group", "Please select group");
        }
        Semester semester = ((Pair) o).getSemester();
        if(semester.getId() == -1){
            errors.rejectValue("semester", "semester", "Please select semester");
        }
    }
    
}

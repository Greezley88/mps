/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package od.onpu.mps.validators.admin;

import od.onpu.mps.entity.Group;
import od.onpu.mps.entity.Student;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author maxim
 */
public class StudentValidator implements Validator{

    @Override
    public boolean supports(Class<?> type) {
        return Student.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "name", "Name must be not empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surname", "surname", "Surname must be not empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "patronymic", "patronymic", "Patronymic must be not empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "number", "number", "Number must be not empty");
        Group curator = ((Student) o).getGroup();
        if(curator.getId() == -1){
            errors.rejectValue("group", "group", "Please select group");
        }
    
    }
    
}

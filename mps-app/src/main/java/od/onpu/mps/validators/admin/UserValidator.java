package od.onpu.mps.validators.admin;


import od.onpu.mps.dto.UsersDTO;

import od.onpu.mps.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created with IntelliJ IDEA.
 * User: StasMenshykov
 * Date: 5/28/13
 * Time: 5:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserValidator implements Validator {

    @Autowired
    UserService userService;

    @Override
    public boolean supports(Class<?> type) {
        return UsersDTO.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "login", "Login must not be empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "password", "Password must not be empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passwordCheck", "passwordCheck", "Please, confirm password");
        String login = ((UsersDTO)o).getLogin();
        if(login != null) {
        if(!userService.isLoginUnique(login))  errors.rejectValue("login", "login", "This login is not unique");
        }
        String password = ((UsersDTO)o).getPassword();
        String passwordCheck = ((UsersDTO)o).getPasswordCheck();
        if (!password.equals(passwordCheck) && !password.equals("") && !passwordCheck.equals("")){

            errors.rejectValue("password", "password", "Different passwords");

        }


    }
    }



package od.onpu.mps.validators.admin;

import od.onpu.mps.dto.EditUsersDTO;
import od.onpu.mps.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created with IntelliJ IDEA.
 * User: Станислав
 * Date: 6/9/13
 * Time: 2:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class EditUserValidator implements Validator {

    @Override
    public boolean supports(Class<?> type) {
        return EditUsersDTO.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o,Errors errors){

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "password", "Password must not be empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passwordCheck", "passwordCheck", "Please, confirm password");

        String password = ((EditUsersDTO)o).getPassword();
        String passwordCheck = ((EditUsersDTO)o).getPasswordCheck();
        if (!password.equals(passwordCheck) && !password.equals("") && !passwordCheck.equals("")){

            errors.rejectValue("password", "password", "Different passwords");

        }
    }
}

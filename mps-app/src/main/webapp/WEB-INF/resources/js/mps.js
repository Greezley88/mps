$(document).ready(function(){

});

var host=null;

function setHost(path){
    host = path;
}

function getHost(){
    return host;
}

function toggleBlock(name){
    var block = $(name);
    if(block.css("display") == 'none'){
        block.css("display","block");
    }else{
        block.css("display","none");
    }
}

function groupSelected(group){

    var value = $(group).val();// id
    toogleAjaxLoder();
    $.getJSON(host+"/table/groupSelected",{groupId:value},function (students){
        updateStudentTable(students);
        toogleAjaxLoder();
    });
}

function userGroupSelected(group){

    var value = $(group).val();// id
    toogleAjaxLoder();
    $.getJSON(host+"/admin/user/groupSelected",{groupId:value},function (students){
        var select = $("#student");
        select.find("option[value!=-1]").remove();

        $.each(students,function(i,student){

                     $("<option />",{
                        val: student.id,
                        text: student.shortFullName
                    }).appendTo(select);
                });
        toogleAjaxLoder();
    });
}



 function statusSelected(status){
    var value = $(status).val();
    if(value != 3){
    $("#btn").attr("disabled","disabled");
    $( '.btn' ).css( 'background', '#819dd1' );
    }
    else {
    $("#btn").removeAttr("disabled");
    $( '.btn' ).css( 'background', '#3259a2' );
    }
    toogleAjaxLoder();
    $.get(host+"/admin/user/status",{stat:value},function (userView){
            $("#userStatus").empty();
            $("#userStatus").append(userView);
            toogleAjaxLoder();
        });
 }

// handler for selection group on schedule page
function updateSemesterList(group){
    var value = $(group).val();// id
    toogleAjaxLoder();
    $.getJSON(host+"/schedule/groupSemesters",{groupId:value},function (semesters){
        var select = $("#semester");
        select.find("option[value!=0]").remove(); // remove all items except default
        $("#schedule").empty(); // clear schedule when group changed
        $.each(semesters,function(i,semester){

             $("<option />",{
                val: semester.id,
                text: semester.name
            }).appendTo(select);
        });
        toogleAjaxLoder();
    });
}

function turnButton(id){
var value = $(id).val();
if(value != -1){
$("#btn").removeAttr("disabled");
$( '.btn' ).css( 'background', '#3259a2' );
}
else {
$("#btn").attr("disabled","disabled");
$( '.btn' ).css( 'background', '#819dd1' );
}

}

function updatePairsList(semester){
    var semesterId = $(semester).val();// id
    var groupId = $("select[name=group]").val();
    toogleAjaxLoder();
    $.getJSON(host+"/table/pairList",{groupId:groupId,semesterId:semesterId},function (pairs){
        var select = $("#pair");
        select.find("option[value!=0]").remove(); // remove all items except default

        $.each(pairs,function(i,pair){

            $("<option />",{
                val: pair.id,
                text: pair.name
            }).appendTo(select);
        });
        toogleAjaxLoder();
    });

}

function pairSelected(pair,editable){
    var pairId = $(pair).val();// id
    var groupId = $("select[name=group]").val();

    if(groupId==0 || pairId==0) return;
    toogleAjaxLoder();
    $.post(host+"/table/pairRating",{groupId:groupId,pairId:pairId,editable:editable},function(ratingView){

        $("#rating").empty();
        $("#rating").append(ratingView);

        toogleAjaxLoder();
    });
}

function updateFinalRating(semester){
    var semesterId = $(semester).val();// id
    var groupId = $("select[name=group]").val();

    if(groupId==0 || semesterId==0) return;
    toogleAjaxLoder();
    $.post(host+"/table/finalRating",{groupId:groupId,semesterId:semesterId},function(ratingView){

        $("#rating").empty();
        $("#rating").append(ratingView);

        toogleAjaxLoder();
    });

}

function updateSchedule(semester){
    var semesterId = $(semester).val();// id
    var groupId = $("select[name=group]").val();

    if(semesterId==0 || groupId==0) return;
    toogleAjaxLoder();
    $.get(host+"/schedule/showSchedule",{groupId:groupId,semesterId:semesterId},function(scheduleView){

        $("#schedule").empty();
        $("#schedule").append(scheduleView);

        toogleAjaxLoder();
    });
}

// clear table and fill with new values
function updateStudentTable(students){

    var table = $("#studentTable tbody");
    table.empty();

    $.each(students,function(i,student){
       var tr = $("<tr />").append(
           $("<td />",{text:i+1}),
           $("<td />",{text:student.fullName}),
           $("<td />",{text:student.number})
       );
       table.append(tr);
    });
}

function submitRatingSettings(form){
    toogleAjaxLoder();
    $.post(host+"/table/saveSettings",$(form).serialize(),function(data){
         pairSelected("#pair"); // update page
    });
    toogleAjaxLoder();
    return false;
}

function ratingChanged(input){
    var form =  $(input);
    var id = form.next().val();
    var val = form.val();
    form.removeClass("error");
    //alert("changed " + val +" id ="+id);
    if(isNaN(val)){
        form.addClass("error");
        return;
    }
    toogleAjaxLoder();
    $.post(host+"/table/saveRating",{ratingId:id,rating:val});
    toogleAjaxLoder();
}

var ajaxLoaderFlag = 0; // global ajax-loader flag of visibility
function toogleAjaxLoder(){
    var loader = $("#ajax-loader");
      if(ajaxLoaderFlag==0){
        loader.css("display","block");  // show ajax-loader
        ajaxLoaderFlag =1;
      }else{
          loader.css("display","none"); // hide ajax-loader
          ajaxLoaderFlag = 0;
      }
}
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="mps" uri="/WEB-INF/taglib/mps-functions.tld" %>
<div class="submenu">
    <ul>
        <li> <a href="${pageContext.request.contextPath}/table/pair" ${mps:activeLink(requestScope['javax.servlet.forward.servlet_path'],"pair")}>По предмету</a></li>
        <li> <a href="${pageContext.request.contextPath}/table/final" ${mps:activeLink(requestScope['javax.servlet.forward.servlet_path'],"final")}>Итоговая</a></li>
    </ul>
</div>
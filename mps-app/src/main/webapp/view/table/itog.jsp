<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="filtr">

    Группа: <select name="group" onchange="updateSemesterList(this)">
        <option value="0">Выбрать</option>
        <c:forEach items="${groups}" var="group">
            <option value="${group.id}">
                <c:out value="${group.name}" />
            </option>
        </c:forEach>
    </select>

    Семестр: <select name="semester" id="semester" onchange="updateFinalRating(this)">
        <option value="0">Выбрать</option>
    </select>

</div>
<div id="rating"></div>
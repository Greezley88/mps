<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="filtr">
    Кафедра: <select name="" >
    <option value="1">КС</option>
    <option value="2">КСС</option>
    <option value="3">АСУ</option>
</select>

    Группа: <select name="group" onchange="groupSelected(this)">
          <c:forEach items="${groups}" var="group">
              <option value="${group.id}" <c:if test="${defaultGroup!=null && defaultGroup==group.id}">selected="selected"</c:if>  >
                  <c:out value="${group.name}" />
              </option>
          </c:forEach>
    </select>
</div>

<table id="studentTable" class="table">
    <thead>
    <tr>
        <td>№</td>
        <td>ФИО</td>
        <td><abbr title="Номер зачетной книжки или студенческого билета">Идентефикатор</abbr></td>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${students}" var="student" varStatus="status">
        <tr>
            <td>${status.count}</td>
            <td>${student.fullName}</td>
            <td>${student.number}</td>
        </tr>
    </c:forEach>

    </tbody>
</table>


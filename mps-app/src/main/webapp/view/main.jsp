<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <title><tiles:getAsString name="title" /></title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/styles.css" />" media="all">

    <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/mps.js" />"></script>

    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/datepicker.css" />" media="all">
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/mps.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/datepicker.js" />"></script>
    <script type="text/javascript">
        setHost("${pageContext.request.contextPath}"); // set host name for using in ajax-requests
    </script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery-1.9.1.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery-ui.js" />"></script>
        <script>
            $(function(){
               $("#days").tabs(); 
            });
        </script>

</head>
<body>
<div id="ajax-loader">
    <img src="<c:url value="/resources/img/ajax-loader.gif" />" />
</div>
<div id="wrapper">
    <div id="header">
        <div id="header_left">
            <h2>Система учета и контроля успеваемости</h2>
        </div>
        <div id="header_right">
            <tiles:insertAttribute name="login_form" />
        </div>
    </div>
    <div id="page">
        <div id="main">
            <tiles:insertAttribute name="menu" />
            <tiles:insertAttribute name="content" />
        </div>
    </div>
    <div id="footer"><div id="foo_in">MPS 2013 &copy; </div></div>
</div>
</body>
</html>

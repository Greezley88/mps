<%-- 
    Document   : addlesson
    Created on : 21.04.2013, 20:39:35
    Author     : maxim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <c:if test="${editable}">
        <h1>Редактировать предмет</h1>
    </c:if>
    <c:if test="${!editable}">
        <h1>Добавить предмет</h1>
    </c:if>
    <div>
        <form:form method="post" action="/admin/lesson/add" commandName="lesson">
            <p><form:hidden path="id" /></p>
            <table>
                <tbody>
                    <tr>
                        <td>Название предмета:</td>
                        <td><form:input path="name"/></td>
                        <td><form:errors path="name" cssClass="error"/></td>
                    </tr>
                </tbody>
            </table>
            <c:if test="${editable}">
                <input type="submit" value="Редактировать"/>
            </c:if>
            <c:if test="${!editable}">
                <input type="submit" value="Добавить"/>
            </c:if>
        </form:form>
    </div>

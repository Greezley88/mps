<%-- 
    Document   : addstudent
    Created on : 24.04.2013, 18:05:57
    Author     : maxim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <c:if test="${editable}">
        <h1>Редактировать студента</h1>
    </c:if>
    <c:if test="${!editable}">
        <h1>Добавить студента</h1>
    </c:if>
        <div>
        <form:form method="post" action="/admin/student/add" commandName="student">
            <p><form:hidden path="id" /></p>
            <table>
                <tbody>
                    <tr>
                        <td>Фамилия:</td>
                        <td><form:input path="surname"/></td>
                        <td><form:errors path="surname" cssClass="error"/></td>
                    </tr>
                    <tr>
                        <td>Имя:</td>
                        <td><form:input path="name"/></td>
                        <td><form:errors path="name" cssClass="error"/></td>
                    </tr>
                    <tr>
                        <td>Отчество:</td>
                        <td><form:input path="patronymic"/></td>
                        <td><form:errors path="patronymic" cssClass="error"/></td>
                    </tr>
                    <tr>
                        <td>Номер зачетки:</td>
                        <td><form:input path="number"/></td>
                        <td><form:errors path="number" cssClass="error"/></td>
                    </tr>
                    <tr>
                        <td>Группа:</td>
                        <td><form:select path="group.id">
                            <form:option value="-1" label="---Select---"/>
                            <form:options items="${groups}" itemValue="id" itemLabel="name"/>
                        </form:select></td>
                        <td><form:errors path="group" cssClass="error"/></td>
                    </tr>
                </tbody>
            </table>
            <c:if test="${editable}">
                <input type="submit" value="Редактировать"/>
            </c:if>
            <c:if test="${!editable}">
                <input type="submit" value="Добавить"/>
            </c:if>
        </form:form>
    </div>

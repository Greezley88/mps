<%-- 
    Document   : addsemester
    Created on : 19.05.2013, 22:40:33
    Author     : maxim
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <c:if test="${editable}">
        <h1>Редактировать семестр</h1>
    </c:if>
    <c:if test="${!editable}">
        <h1>Добавить семестр</h1>
    </c:if>
    <div>
        <form:form method="post" action="/admin/semester/add" commandName="semester">
            <p><form:hidden path="id" /></p>
            <table>
                <tbody>
                    <tr>
                        <td>Начало семестра:</td>
                        <td>
                            <form:input path="startDate" readonly="true"/>
                            <input type="button" style="background: url('<c:url value="/resources/img/datepicker.jpg" />') no-repeat;
                                   width: 30px; border: 0px;" 
                                   onclick="displayDatePicker('startDate', false, 'ymd', '-');"></td>
                        <td><form:errors path="startDate" cssClass="error"/></td>
                    </tr>
                    <tr>
                        <td>Конец семестра:</td>
                        <td><form:input path="endDate" readonly="true"/>
                            <input type="button" style="background: url('<c:url value="/resources/img/datepicker.jpg" />') no-repeat;
                                   width: 30px; border: 0px;" 
                                   onclick="displayDatePicker('endDate', false, 'ymd', '-');"></td>
                        <td><form:errors path="endDate" cssClass="error"/></td>
                    </tr>
                </tbody>
            </table>
            <c:if test="${editable}">
                <input type="submit" value="Редактировать"/>
            </c:if>
            <c:if test="${!editable}">
                <input type="submit" value="Добавить"/>
            </c:if>
        </form:form>
    </div>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>



        <h1>Добавить пользователя</h1>

        <div class="filtr">
        <form:form method="post" action="/admin/user/add" commandName="usersDTO">

            <table>
                <tbody>
                    <tr>
                        <td>Логин:</td>
                        <td><form:input path="login"/></td>
                        <td><form:errors path="login" cssClass="error"/></td>
                    </tr>
                    <tr>
                        <td>Пароль:</td>
                        <td><form:input type="password" path="password"/></td>
                        <td><form:errors path="password" cssClass="error"/></td>
                    </tr>
                    <tr>
                        <td>Подтвердите пароль:</td>
                        <td><form:input type="password" path="passwordCheck"/></td>
                        <td><form:errors path="passwordCheck" cssClass="error"/></td>
                                        </tr>
                    <tr>
                    <td>Статус:</td>
                    <td><select name="status" id="status" onchange="statusSelected(this)">
                                                                                          <option value="3">
                                                                                              <c:out value="Простой пользователь" />
                                                                                          </option>
                                                                                          <option value="1">
                                                                                              <c:out value="Студент" />
                                                                                          </option>
                                                                                           <option value="2">
                                                                                              <c:out value="Преподаватель" />
                                                                                          </option>

                                                                                </select> </td>

                    </tr>
                    <tr>
                    <td>
                    Роли:
                    </td>
                    <td>
                    <form:checkbox value="3" label="Администратор" path="roles"/><br/>
                    <form:checkbox value="4" label="Секретарь" path="roles"/><br/>
                    </td>
                    </tr>



                </tbody>
            </table>

            <table>
            <tbody>
             <tr id="userStatus">
             </tr>
             </tbody>
             </table>

             <input id="btn" type="submit" value="Добавить" class="btn"/>

        </form:form>




    </div>










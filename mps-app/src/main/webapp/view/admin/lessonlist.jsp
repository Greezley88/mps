<%-- 
    Document   : lessonlist
    Created on : 21.04.2013, 20:39:11
    Author     : maxim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <h1>Список предметов</h1>
    <div class="error">${message}</div>
    <c:forEach items="${lessons}" var="lesson">
        <c:out value="${lesson.id}"/>.
        <c:out value="${lesson.name}"/>
        <a href="<c:url value="/admin/lesson/edit/${lesson.id}"/> " >Редактировать</a>
        <a href="<c:url value="/admin/lesson/delete/${lesson.id}" />">Удалить</a>
        <br/>
    </c:forEach>
    <a href="<c:url value="/admin/lesson/add"/>">Добавить новый предмет</a>


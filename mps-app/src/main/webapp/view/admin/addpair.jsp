<%-- 
    Document   : addpair
    Created on : 25.04.2013, 20:21:28
    Author     : maxim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <c:if test="${editable}">
        <h1>Редактировать пару</h1>
    </c:if>
    <c:if test="${!editable}">
        <h1>Добавить пару</h1>
    </c:if>
    <form:form method="post" action="/admin/pair/add" commandName="pair">
        <form:hidden path="id"/>
        <table>
            <tbody>
                <tr>
                    <td>Предмет:</td>
                    <td><form:select path="lesson.id">
                        <form:option value="-1" label="---Select---"/>
                        <form:options items="${lessons}" itemValue="id" itemLabel="name"/>
                    </form:select></td>
                    <td><form:errors path="lesson" cssClass="error"/></td>
                </tr>
                <tr>
                    <td>Преподаватель:</td>
                    <td><form:select path="teacher.id">
                        <form:option value="-1" label="---Select---"/>
                        <form:options items="${teachers}" itemValue="id" itemLabel="fullName"/>
                    </form:select></td>
                    <td><form:errors path="teacher" cssClass="error"/></td>
                </tr>
                <tr>
                    <td>Группа:</td>
                    <td><form:select path="group.id">
                        <form:option value="-1" label="---Select---"/>
                        <form:options items="${groups}" itemValue="id" itemLabel="name"/>
                    </form:select></td>
                    <td><form:errors path="group" cssClass="error"/></td>
                </tr>
                <tr>
                    <td>Номер пары:</td>
                    <td><form:input path="number"/></td>
                    <td></td>
                </tr>
                <tr>
                    <td>День недели:</td>
                    <td><form:input path="weekDay"/></td>
                    <td><form:errors path="weekDay" cssClass="error"/></td>
                </tr>
                <tr>
                    <td>Семестр:</td>
                    <td><form:select path="semester.id">
                        <form:option value="-1" label="---Select---"/>
                        <form:options items="${semesters}" itemValue="id" itemLabel="name"/>
                    </form:select></td>
                    <td><form:errors path="semester" cssClass="error"/></td>
                </tr>
                <tr>
                    <td>Кабинет:</td>
                    <td><form:input path="cabinet"/></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Четность:</td>
                    <td><form:select path="parity">
                        <form:option value="0" label="-"/>
                        <form:option value="1" label="Неч"/>
                        <form:option value="2" label="Чет"/>
                    </form:select></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <c:if test="${editable}">
            <input type="submit" value="Редактировать"/>
        </c:if>
        <c:if test="${!editable}">
            <input type="submit" value="Добавить"/>
        </c:if>
    </form:form>

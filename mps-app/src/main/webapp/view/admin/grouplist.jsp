<%-- 
    Document   : grouplist
    Created on : 17.04.2013, 21:57:16
    Author     : maxim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

        <h1>Список групп</h1>
        <div class="error">${message}</div><br/>
        <c:forEach items="${groups}" var="group">
            <c:out value="${group.id}"/>. 
                <b><c:out value="${group.name}"/></b> - 
                <c:out value="${group.curator.fullName}"/>
                <a href="<c:url value="/admin/group/edit/${group.id}"/>">Редактировать</a>
                <a href="<c:url value="/admin/group/delete/${group.id}"/>">Удалить</a>
                <a href="<c:url value="/admin/student/list/${group.id}"/>">Просмотр</a>
                <a href="<c:url value="/admin/pair/list/${group.id}"/>">Расписание</a><br/>
        </c:forEach>
        <a href="<c:url value="/admin/group/add"/>">Добавить новую группу</a>

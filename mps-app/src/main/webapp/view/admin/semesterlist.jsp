<%-- 
    Document   : semesterlist
    Created on : 19.05.2013, 22:36:15
    Author     : maxim
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <h1>Список семестров</h1>
    <div class="error">${message}</div><br/>
    <c:forEach items="${semesters}" var="semester">
        <c:out value="${semester.id}"/>.
        <b><c:out value="${semester.name}"/></b>
        <a href="<c:url value="/admin/semester/edit/${semester.id}" />">Редактировать</a>
        <a href="<c:url value="/admin/semester/delete/${semester.id}"/>">Удалить</a><br/>
    </c:forEach>
    <a href="<c:url value="/admin/semester/add"/>">Добавить новый семестр</a>

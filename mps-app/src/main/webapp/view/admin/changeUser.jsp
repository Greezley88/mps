<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h1>Смена пароля</h1>

 <div class="filtr">
 <form:form method="post" action="/admin/user/edit/${editUser.id}" commandName="editUser">
 <table>
                 <tbody>
                     <tr>
                         <td>Пароль:</td>
                         <td><form:input type="password" path="password"/></td>
                         <td><form:errors path="password" cssClass="error"/></td>
                     </tr>
                     <tr>
                         <td>Подтвердите пароль:</td>
                         <td><form:input type="password" path="passwordCheck"/></td>
                         <td><form:errors path="passwordCheck" cssClass="error"/></td>
                     </tr>

                     <tr>
                     <td></td>
                     <td> <input id="btn" type="submit" value="Изменить" class="btn" /></td>
                     </tr>

                 </tbody>
</table>
 </form:form>
 </div>
<%-- 
    Document   : teacherlist
    Created on : 17.04.2013, 20:58:57
    Author     : maxim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <h1>Список преподователей</h1>
    <div class="error">${message}</div><br/>
    <c:forEach items="${teachers}" var="teacher">
        <c:out value="${teacher.id}"/>.
        <b><c:out value="${teacher.fullName}"/></b> -
        <c:out value="${teacher.rank}"/>
        <a href="<c:url value="/admin/teacher/edit/${teacher.id}" />">Редактировать</a>
        <a href="<c:url value="/admin/teacher/delete/${teacher.id}"/>">Удалить</a><br/>
    </c:forEach>
    <a href="<c:url value="/admin/teacher/add"/>">Добавить нового преподавателя</a>

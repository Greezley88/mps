<%-- 
    Document   : pairlist
    Created on : 25.04.2013, 20:15:42
    Author     : maxim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <h1>Расписание для группы ${groupName}</h1>
    <div class="error">${message}</div><br/>
    <form action="<c:url value="/admin/pair/selectsemester"/>" method="post">
        <input type="text" id="group" name="group" value="${groupId}" hidden="hidden"/>
        Семестр: <select name="semester" id="semester">
            <c:forEach items="${semesters}" var="semester">
                <option value="${semester.id}">${semester.name}</option>
            </c:forEach>
        </select>
        <input type="submit" value="Обновить"/>
    </form>
    <div id="days">
        <ul>
            <li><a href="#pn">Понедельник</a></li>
            <li><a href="#vt">Вторник</a></li>
            <li><a href="#sr">Среда</a></li>
            <li><a href="#ht">Четверг</a></li>
            <li><a href="#pt">Пятница</a></li>
        </ul>
        <div id="pn">
            <c:forEach items="${mon}" var="pair">
                <c:out value="${pair.id}"/>. 
                    <b><c:out value="${pair.lesson.name}"/></b> - 
                    <c:out value="${pair.teacher.shortFullName}"/>
                    <c:out value="${pair.group.name}"/>
                    <c:out value="${pair.number}"/>
                    <c:out value="${pair.weekDay}"/>
                    <c:out value="${pair.semester.name}"/>
                    <c:out value="${pair.cabinet}"/>
                    <c:out value="${pair.parity}"/>
                    <a href="<c:url value="/admin/pair/delete/${pair.id}" />">Удалить</a><br/>
            </c:forEach>
        </div>
        <div id="vt">
            <c:forEach items="${tue}" var="pair">
                <c:out value="${pair.id}"/>. 
                    <b><c:out value="${pair.lesson.name}"/></b> - 
                    <c:out value="${pair.teacher.shortFullName}"/>
                    <c:out value="${pair.group.name}"/>
                    <c:out value="${pair.number}"/>
                    <c:out value="${pair.weekDay}"/>
                    <c:out value="${pair.semester.name}"/>
                    <c:out value="${pair.cabinet}"/>
                    <c:out value="${pair.parity}"/>
                    <a href="<c:url value="/admin/pair/delete/${pair.id}" />">Удалить</a><br/>
            </c:forEach>
        </div>
        <div id="sr">
            <c:forEach items="${wed}" var="pair">
                <c:out value="${pair.id}"/>. 
                    <b><c:out value="${pair.lesson.name}"/></b> - 
                    <c:out value="${pair.teacher.shortFullName}"/>
                    <c:out value="${pair.group.name}"/>
                    <c:out value="${pair.number}"/>
                    <c:out value="${pair.weekDay}"/>
                    <c:out value="${pair.semester.name}"/>
                    <c:out value="${pair.cabinet}"/>
                    <c:out value="${pair.parity}"/>
                    <a href="<c:url value="/admin/pair/delete/${pair.id}" />">Удалить</a><br/>
            </c:forEach>
        </div>
        <div id="ht">
            <c:forEach items="${thu}" var="pair">
                <c:out value="${pair.id}"/>. 
                    <b><c:out value="${pair.lesson.name}"/></b> - 
                    <c:out value="${pair.teacher.shortFullName}"/>
                    <c:out value="${pair.group.name}"/>
                    <c:out value="${pair.number}"/>
                    <c:out value="${pair.weekDay}"/>
                    <c:out value="${pair.semester.name}"/>
                    <c:out value="${pair.cabinet}"/>
                    <c:out value="${pair.parity}"/>
                    <a href="<c:url value="/admin/pair/delete/${pair.id}" />">Удалить</a><br/>
            </c:forEach>
        </div>
        <div id="pt">
            <c:forEach items="${fri}" var="pair">
                <c:out value="${pair.id}"/>. 
                    <b><c:out value="${pair.lesson.name}"/></b> - 
                    <c:out value="${pair.teacher.shortFullName}"/>
                    <c:out value="${pair.group.name}"/>
                    <c:out value="${pair.number}"/>
                    <c:out value="${pair.weekDay}"/>
                    <c:out value="${pair.semester.name}"/>
                    <c:out value="${pair.cabinet}"/>
                    <c:out value="${pair.parity}"/>
                    <a href="<c:url value="/admin/pair/delete/${pair.id}" />">Удалить</a><br/>
            </c:forEach>
        </div>
    </div>
    <a href="<c:url value="/admin/pair/add/${groupId}/${semesterId}" />">Добавить новую пару</a>

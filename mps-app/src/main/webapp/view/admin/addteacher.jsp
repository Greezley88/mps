<%-- 
    Document   : addtescher
    Created on : 17.04.2013, 19:43:54
    Author     : maxim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <c:if test="${editable}">
        <h1>Редактировать преподователя</h1>
    </c:if>
    <c:if test="${!editable}">
        <h1>Добавить преподователя</h1>
    </c:if>
    <div>
        <form:form method="post" action="/admin/teacher/add" commandName="teacher">
            <p><form:hidden path="id" /></p>
            <table>
                <tbody>
                    <tr>
                        <td>Фамилия:</td>
                        <td><form:input path="surname"/></td>
                        <td><form:errors path="surname" cssClass="error"/></td>
                    </tr>
                    <tr>
                        <td>Имя:</td>
                        <td><form:input path="name"/></td>
                        <td><form:errors path="name" cssClass="error"/></td>
                    </tr>
                    <tr>
                        <td>Отчество:</td>
                        <td><form:input path="patronymic"/></td>
                        <td><form:errors path="patronymic" cssClass="error"/></td>
                    </tr>
                    <tr>
                        <td>Звание:</td>
                        <td><form:input path="rank"/></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <c:if test="${editable}">
                <input type="submit" value="Редактировать"/>
            </c:if>
            <c:if test="${!editable}">
                <input type="submit" value="Добавить"/>
            </c:if>
        </form:form>
    </div>

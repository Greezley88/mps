<%-- 
    Document   : addgroup
    Created on : 17.04.2013, 22:02:39
    Author     : maxim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

        <c:if test="${editable}">
            <h1>Редактировать группу</h1>
        </c:if>
        <c:if test="${!editable}">
            <h1>Добавить группу</h1>
        </c:if>
        <form:form method="post" action="/admin/group/add" commandName="group">
            <form:hidden path="id"/>
            <table>
                <tbody>
                    <tr>
                        <td>Название группы:</td>
                        <td><form:input path="name"/></td>
                        <td><form:errors path="name" cssClass="error"/></td>
                    </tr>
                    <tr>
                        <td>Куратор:</td>
                        <td><form:select path="curator.id">
                            <form:option value="-1" label="---Select---"/>
                            <form:options items="${teachers}" itemValue="id" itemLabel="fullName"/>
                        </form:select></td>
                        <td><form:errors path="curator" cssClass="error"/></td>
                    </tr>
                </tbody>
            </table>
            <c:if test="${editable}">
                <input type="submit" value="Редактировать"/>
            </c:if>
            <c:if test="${!editable}">
                <input type="submit" value="Добавить"/>
            </c:if>
        </form:form>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

        <h1>Управление пользователями</h1>
        <div class="error">${message}</div><br/>

        <div>
        <form action="search"  method="post">
        <table>
          <tbody>
                            <tr>
                                <td>Поиск:</td>
                                <td><input  name="name"/></td>
                            </tr>
          </tbody>
        </table>
                            <input id="btn" type="submit" value="Найти" class="btn"/>
        </form>
        </div>
        <c:forEach items="${users}" var="user">
        <c:if test ="${user.teacher != null}">
        <c:out value="${user.teacher.surname}"/>
        <c:out value="${user.teacher.name}"/>
        <c:out value="${user.teacher.patronymic}"/> -
        <c:out value="${user.teacher.rank}"/>.
        </c:if>

        <c:if test ="${user.student != null}">
        <c:out value="${user.student.surname}"/>
        <c:out value="${user.student.name}"/>
        <c:out value="${user.student.patronymic}"/> -
        <c:out value="${user.student.group.name}"/>.
        </c:if>

        Login: <c:out value="${user.login}"/>

        <a href="<c:url value="/admin/user/edit/${user.id}"/>">Изменить пароль</a>
        <a href="<c:url value="/admin/user/delete/${user.id}"/>">Удалить</a><br/>
         </c:forEach>

        <a href="<c:url value="/admin/user/add"/>">Добавить нового пользователя</a>
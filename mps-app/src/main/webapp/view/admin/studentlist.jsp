<%-- 
    Document   : studentlist
    Created on : 24.04.2013, 18:14:38
    Author     : maxim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <h1>Список студентов</h1>
    <div class="error">${message}</div><br/>
    <c:forEach items="${students}" var="student">
        <c:out value="${student.id}"/>.
        <b><c:out value="${student.name}"/>
        <c:out value="${student.patronymic}"/>
        <c:out value="${student.surname}"/></b>
        <c:out value="${student.number}"/> -
        <c:out value="${student.group.name}"/>
        <a href="<c:url value="/admin/student/edit/${student.id}" />" >Редактировать</a>
        <a href="<c:url value="/admin/student/delete/${student.id}" />">Удалить</a><br/>
    </c:forEach>
    <a href="<c:url value="/admin/student/add" />">Добавить нового студента</a>


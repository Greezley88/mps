<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <title><tiles:getAsString name="title" /></title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/styles.css" />" media="all">
</head>
<body>

<div id="wrapper">
    <div id="header">
        <div id="header_left">
<h1>Залогиньтесь, сэр</h1>
</div>
        <div id="header_right">
            <tiles:insertAttribute name="login_form" />
        </div>
    </div>
    <div id="page">
    </div>
    <div id="footer"><div id="foo_in">MPS 2013 &copy; </div></div>
</div>
</body>
</html>
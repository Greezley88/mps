<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="filtr">

    Группа: <select name="group" onchange="updateSemesterList(this)">
        <c:forEach items="${groups}" var="group">
            <option value="${group.id}" <c:if test="${defaultGroup!=null && defaultGroup==group.id}">selected="selected"</c:if>  >
            <c:out value="${group.name}" />
            </option>
        </c:forEach>
    </select>

    Семестр: <select name="semester" id="semester" onchange="updateSchedule(this)">
        <option value="0">Выбрать</option>
        <c:forEach items="${semesters}" var="sem">
            <option value="${sem.id}">${sem.name}</option>
        </c:forEach>
    </select>

</div>

<div id="schedule">
     <c:forEach var="entry" items="${schedule}">
         <div>
             <h3><spring:message code="weekDay.${entry.key}" /></h3>
             <table class="table">
                 <tbody>
                 <c:forEach items="${entry.value}" var="pair">
                     <tr>
                         <td>${pair.number}.<spring:message code="parity.${pair.parity}" /> </td>
                         <td> <b>${pair.lesson.name}</b> <br/>${pair.teacher.shortFullName}<br /><div class="right">${pair.cabinet}</div></td>
                     </tr>
                 </c:forEach>

                 </tbody>
             </table>
         </div>
     </c:forEach>
</div>
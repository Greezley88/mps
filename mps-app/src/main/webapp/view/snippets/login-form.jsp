<%--
  Created by IntelliJ IDEA.
  User: ppavlenko
  Date: 21.04.13
  Time: 20:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:url value="/j_spring_security_check"  var="security_check"/>
<c:if test = "${userMain == null}">
<form action="${security_check}" method="post">
    <input type="text" name="j_username" placeholder="Логин" />
    <input type="password" name="j_password" placeholder="Пароль" />
    <input type="submit" value="Вход" class="btn"/>
</form>
</c:if>
<c:if test = "${userMain != null}">
<h3>Пользователь: ${userDetails}</h3>
<c:url value="/logout" var="logoutUrl"/>
<a href="${logoutUrl}">Log Out</a>
</c:if>
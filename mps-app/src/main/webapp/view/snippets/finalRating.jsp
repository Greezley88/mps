<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:if test="${students[0]!=null}" >
    <div id="ratingTable">
        <table id="studentTable" class="table">
            <thead>
            <tr>
                <td>№</td>
                <td>Ф.И.О.</td>
                <c:forEach items="${students[0].rating}" var="rat" >
                    <td>${rat.pair.name}</td>
                </c:forEach>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${students}" var="student" varStatus="status">
                <tr>
                    <td>${status.count}</td>
                    <td>${student.student.shortFullName}</td>
                    <c:forEach items="${student.rating}" var="rat" >
                        <td>${rat.rating}</td>
                    </c:forEach>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</c:if>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:forEach var="entry" items="${schedule}">
    <div>
        <h3><spring:message code="weekDay.${entry.key}" /></h3>
        <table class="table">
            <tbody>
            <c:forEach items="${entry.value}" var="pair">
                <tr>
                    <td>${pair.number}.<spring:message code="parity.${pair.parity}" /></td>
                    <td> <b>${pair.lesson.name}</b> <br/>${pair.teacher.shortFullName}<br /><div class="right">${pair.cabinet}</div></td>
                </tr>
            </c:forEach>

            </tbody>
        </table>
    </div>
</c:forEach>
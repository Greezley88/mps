<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="mps" uri="/WEB-INF/taglib/mps-functions.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div id="menu">
    <ul>
        <li> <a href="${pageContext.request.contextPath}/index" ${mps:activeLink(requestScope['javax.servlet.forward.servlet_path'],"index")}>Главная</a></li>
        <c:if test = "${showTable}">
        <li> <a href="${pageContext.request.contextPath}/table" ${mps:activeLink(requestScope['javax.servlet.forward.servlet_path'],"table")}>Ведомость</a></li>
        </c:if>
        <li> <a href="${pageContext.request.contextPath}/schedule" ${mps:activeLink(requestScope['javax.servlet.forward.servlet_path'],"schedule")}>Расписание</a></li>
        <c:if test = "${isAdmin}">
        <li> <a href="${pageContext.request.contextPath}/admin" ${mps:activeLink(requestScope['javax.servlet.forward.servlet_path'],"admin")}>Панель администратора</a></li>
        </c:if>
        <li><a href="#">Поиск</a></li>
    </ul>
</div>
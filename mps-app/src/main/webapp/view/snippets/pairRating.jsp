<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="mps" uri="/WEB-INF/taglib/mps-functions.tld" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<h3>Статистика предмета ${pair.name} группа: ${pair.group.name}</h3>


<c:if test="${students[0]!=null}" >
<c:if test="${turnOn == true}">
    <div class="right"><button onclick="pairSelected('#pair',1)" class="btn">Редактировать</button></div>
</c:if>
    <div id="ratingTable">
        <table class="table">
            <thead>
            <tr>
                <td>№</td>
                <td>Ф.И.О</td>
                <c:forEach items="${students[0].rating}" var="rat" >
                    <td ${mps:ratingCssClass(rat.type)}>${mps:formatDate(rat.ratingDate)}</td>
                </c:forEach>
            </tr>
            </thead>
            <tbody>

            <c:forEach items="${students}" var="student" varStatus="status">
                <tr>
                    <td>${status.count}</td>
                    <td>${student.student.shortFullName}</td>
                    <c:forEach items="${student.rating}" var="rat" >
                        <td>${rat.rating}</td>
                    </c:forEach>
                </tr>
            </c:forEach>

            </tbody>
        </table>
    </div>
    <div id="editRatingSettings">
        <c:if test="${turnOn == true}">
        <div class="right"><button onclick="toggleBlock('#settings_form')" class="btn">Настройки</button></div>
        </c:if>
        <div id="settings_form" class="hide">
            <form action="" method="post" onsubmit="return submitRatingSettings(this)">
                <table>
                    <tr>
                        <td>Модуль:</td><td>

                        <select name="module" multiple="multiple">
                            <c:forEach items="${students[0].rating}" var="rat" >
                                <option value="${rat.id}" ${mps:selectedOption(rat.type,'MODULAR')}>${mps:formatDate(rat.ratingDate)}</option>
                            </c:forEach>
                    </select></td>
                    </tr>
                    <tr>
                        <td>Итоговый контроль:</td><td><select name="finalRating">
                            <c:forEach items="${students[0].rating}" var="rat" >
                                <option value="${rat.id}" ${mps:selectedOption(rat.type,'FINAL')}>${mps:formatDate(rat.ratingDate)}</option>
                            </c:forEach>
                    </select></td>
                    </tr>
                    <tr><td colspan="2">
                        <input type="submit" value="Сохранить" class="btn"/>
                    </td></tr>
                </table>
                <input type="hidden" name="pair" value="${pair.id}"/>
            </form>
        </div>
    </div>

</c:if>
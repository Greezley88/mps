<%--
  Created by IntelliJ IDEA.
  User: ppavlenko
  Date: 21.04.13
  Time: 20:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="mps" uri="/WEB-INF/taglib/mps-functions.tld" %>
<div id="menu">
    <ul>
        <li> <a href="<c:url value="/admin/group/list" />" ${mps:activeLink(pageName,"groups")}> Группы</a> </li>
        <li> <a href="<c:url value="/admin/teacher/list" />" ${mps:activeLink(pageName,"teachers")}> Преподаватели</a> </li>
        <li> <a href="<c:url value="/admin/lesson/list" />" ${mps:activeLink(pageName,"lessons")}> Уроки</a> </li>
        <li> <a href="<c:url value="/admin/student/all" />" ${mps:activeLink(pageName,"students")}> Студенты</a> </li>
        <li> <a href="<c:url value="/admin/group/list" />" ${mps:activeLink(pageName, "pairs")}> Пары </a> </li>
        <li> <a href="<c:url value="/admin/semester/list" />" ${mps:activeLink(pageName, "semesters")}> Семестры </a> </li>
        <li> <a href="<c:url value="/admin/user/list" />" ${mps:activeLink(pageName, "users")}>Пользователи </a> </li>
        <li> <a href="<c:url value="/index" />">На главную</a> </li>
    </ul>
</div>